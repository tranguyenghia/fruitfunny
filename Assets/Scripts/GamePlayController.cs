﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GamePlayController : MonoBehaviour {
    public static GamePlayController instance;

    public GameObject pausePanel, victoryPanel, completePanel, failPanel, darkPanel, assistPanel;
    public Button restartPauseButton, restartFailButton, restartVictoryButton, restartCompleteButton, nextVictoryButton;
    public Text scoreText, scoreVictory, scoreComplete, bestVictoryScore, scoreFail, bestFailScore, levelText, levelVictory, levelFail, boomText, searchText, swapText;
    public Animator animVictoryPanel, animCompletePanel;
    public int id = new int();
    public Transform canvas;

    public int[,] matrix = new int[11,9];
    public string[,] matrixFruitObject = new string[11, 9];
    public string[,] matrixButtonBGObject = new string[11, 9];
    public int xFruitSelected, yFruitSelected;

    int numberFruit, score = 0, scoreTemp = 0, star, totalLevelMode, assist = 1;
    float timeDelayVictory;
    public bool fail = false, victory = false;
    float timer = 0f;
    private string jsonString, txtString;
    private JsonData menuData, itemData;

    public int gravityType = 0, timerBurn = 3;
    public int boom, search, swap;

    public AudioSource audioSource, audioBackground;
    public AudioClip pressCard, destroyCard, swapSupport, boomSupport, searchSupport, pressButton, failSound, victorySound;

    bool isPause;
    private void Awake()
    {
        ReadFileTXT(GameManager.instance.playMode, GameManager.instance.levelCurrent, GameManager.instance.numberPlayLevel);
        SetMatrix();
    }
    //public Image img;

    // Use this for initialization
    void Start () {
        SoundControl();
        MusicControl();
        UnityEngine.Time.timeScale = 1f;
        MakeInstance();
        boom = 20;
        search = 40;
        swap = 20;
        Load();
        levelText.text = GameManager.instance.levelCurrent.ToString();

        //star = 3;
        //victoryPanel.SetActive(true);
        //darkPanel.SetActive(true);
        //StartCoroutine(ShowStar());
        //Debug.Log("" + PlayerPrefs.GetInt("StarMode1Level5"));

        //Log
        //Vector3 vector3 = GetVectorObjectFruitWithXY(4, 1);
        //Debug.Log("Toa do: 4-1 x: " + vector3.x + " y: " + vector3.y);
        //String nameObject = matrixFruitObject[1, 1];
        //GameObject fruit = GameObject.Find(nameObject);
        //fruit.transform.localPosition = vector3;

    }
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.A))
        {
            //FunctionController.instance.GetArrayCF();
        }
        SetTextSupportButton();
        if (CountFruit() == 0 && !victory)
        {
            if (GameManager.instance.numberPlayLevel < (GameManager.instance.totalPlayLevel - 1))
            {
                NextStep();
            }
            else
            {
                timeDelayVictory += Time.deltaTime * 10;
                if (timeDelayVictory > 7f)
                {
                    if(GameManager.instance.levelCurrent >= totalLevelMode)
                    {
                        Complete();
                    }
                    else
                    {
                        Victory();
                    }
                    
                }
            }
           
            
        }
        if(TimeSliderController.instance != null)
        {  
            if ((TimeSliderController.instance.timer <= 2f) && !fail)
            {
                if (assist > 0)
                {
                    Assist();
                }
                else
                {
                    GameOver();
                }
                
            }
            else
            {
                score = (numberFruit - CountFruit()) * 100;
                scoreText.text = score.ToString("D7");
            }
        }
        else
        {
           
        }
        if (Input.GetKey(KeyCode.Escape) && !victory && !fail)
        {
            PauseGame();
        }
        //if (Input.GetKey(KeyCode.Home) || Input.GetKey(KeyCode.Menu))
        //{
        //    PauseGame();
        //}
        //SetScoreTextPanel();
	}
    public void PauseButton()
    {
        //isPause = true;
        AudioButton(3);
        PauseGame();
    }
    public void OkAssist()
    {
        if (assist > 0)
        {
            assist = 0;
        }
        assistPanel.SetActive(false);
        darkPanel.SetActive(false);
        UnityEngine.Time.timeScale = 1;
        TimeSliderController.instance.timer += 50;
        
    }
    public void CancelAssist()
    {
        assistPanel.SetActive(false);
        darkPanel.SetActive(false);
        GameOver();
    }
    private void OnApplicationPause(bool pause)
    {
        
        //isPause = pause;
        //Debug.Log("Tam dung game: " + isPause);
        if (pause && !isPause)
        {
            if (victory || fail)
            {

            }
            else
            {
                PauseGame();
            }
            
        }
    }
    private void OnApplicationFocus(bool focus)
    {
        //isPause = !focus;
        //Debug.Log("Tam dung game: " + isPause);
    }
    public void PauseGame()
    {
        isPause = true;
        //AudioButton(3);   
        UnityEngine.Time.timeScale = 0f;
        pausePanel.SetActive(true);
        restartPauseButton.onClick.RemoveAllListeners();
        restartPauseButton.onClick.AddListener(() => RestartGame());
 
    }
    public void ResumeGame()
    {
        AudioButton(3);
        isPause = false;
        UnityEngine.Time.timeScale = 1f;
        if (pausePanel.activeSelf == true)
        {
            pausePanel.SetActive(false);
        }
        if(failPanel.activeSelf == true)
        {
            failPanel.SetActive(false);
        }
        
        
    }
    public void BackToMenu()
    {
        GameManager.instance.numberPlayLevel = 0;
        AudioButton(3);
        UnityEngine.Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
        
    }
    private void SetScoreTextPanel()
    {
        if (victory)
        {
            if (scoreTemp < score)
            { 
                timer += UnityEngine.Time.deltaTime * 2000;
                scoreTemp = (int)timer;
                //Debug.Log("Diem tang dan la: " + scoreTemp);
                scoreVictory.text = scoreTemp.ToString("D7");
            }
        }
        if (fail)
        {
            if(scoreTemp <= score)
            {
                timer += UnityEngine.Time.deltaTime * 2000;
                scoreTemp = (int)timer;
                //Debug.Log("Diem tang dan la: " + scoreTemp);
                scoreFail.text = scoreTemp.ToString("D7");
            }
        }
        
    }
    public void RestartGame()
    {
        GameManager.instance.numberPlayLevel = 0;
        UnityEngine.Time.timeScale = 1f;
        Application.LoadLevel(Application.loadedLevel);
        UnityEngine.Time.timeScale = 1f;

    }
    private void NextLevel()
    {
        UnityEngine.Time.timeScale = 1f;
        GameManager.instance.levelCurrent += 1;
        GameManager.instance.numberPlayLevel = 0;
        //set star next level
        if(PlayerPrefs.GetInt("StarMode" + GameManager.instance.playMode.ToString() + "Level" + GameManager.instance.levelCurrent.ToString() ) == -1)
        {
            PlayerPrefs.SetInt("StarMode" + GameManager.instance.playMode.ToString() + "Level" + GameManager.instance.levelCurrent.ToString(), 0);
        }
        if(GameManager.instance.levelCurrent >= totalLevelMode)
        {
            Application.LoadLevel("MainMenu");
        }
        else
        {
            Application.LoadLevel(Application.loadedLevel);
        }
        
        UnityEngine.Time.timeScale = 1f;
    }
    private void NextStep()
    {
        GameManager.instance.numberPlayLevel += 1;
        Application.LoadLevel(Application.loadedLevel);
    }
    private void Assist()
    {
        assistPanel.SetActive(true);
        UnityEngine.Time.timeScale = 0f;       
        darkPanel.SetActive(true);
    }
    private void GameOver()
    {
        GameManager.instance.numberPlayLevel = 0;
        fail = true;
        audioBackground.enabled = false;
        audioSource.PlayOneShot(failSound);
            
        UnityEngine.Time.timeScale = 0f;
        failPanel.SetActive(true);
        darkPanel.SetActive(true);
        //set text level, score, bestScore
        levelFail.text = GameManager.instance.levelCurrent.ToString("D3");
        scoreFail.text = score.ToString("D7");
        if (score > PlayerPrefs.GetInt("ScoreMode" + GameManager.instance.playMode.ToString() + "Level" + GameManager.instance.levelCurrent))
        {
            PlayerPrefs.SetInt("ScoreMode" + GameManager.instance.playMode.ToString() + "Level" + GameManager.instance.levelCurrent, score);
            bestFailScore.text = score.ToString("D7");
        }
        else
        {
            bestFailScore.text = PlayerPrefs.GetInt("ScoreMode" + GameManager.instance.playMode.ToString() + "Level" + GameManager.instance.levelCurrent).ToString("D7");
        }

        restartFailButton.onClick.RemoveAllListeners();
        restartFailButton.onClick.AddListener(() => RestartGame());
        
    }
    private void Complete()
    {
        victory = true;
        float time = TimeSliderController.instance.timer;
        if (time > 30f)
        {
            star = 3;

        }
        else if (time > 20f)
        {
            star = 2;

        }
        else
        {
            star = 1;

        }
        completePanel.SetActive(true);
        darkPanel.SetActive(true);
        StartCoroutine(ShowStar(animCompletePanel));
        SaveStar();
        //set score
        int bonusSocre = (int)time;
        score += bonusSocre * 20;
        scoreComplete.text = score.ToString("D7");

        restartCompleteButton.onClick.RemoveAllListeners();
        restartCompleteButton.onClick.AddListener(() => RestartGame());
    }
    private void Victory()
    {
        GameManager.instance.numberPlayLevel = 0;
        victory = true;
        audioBackground.enabled = false;
        audioSource.PlayOneShot(victorySound);
        
        //victory = true;
        //UnityEngine.Time.timeScale = 0f;
        
        float time = TimeSliderController.instance.timer;
        //set star
        if(time > 30f)
        {
            star = 3;
                      
        }
        else if(time > 20f)
        {
            star = 2;
           
        }
        else
        {
            star = 1;
            
        }
        victoryPanel.SetActive(true);
        darkPanel.SetActive(true);
        StartCoroutine(ShowStar(animVictoryPanel));
        SaveStar();
        //set score, level, best score
        levelVictory.text = GameManager.instance.levelCurrent.ToString("D3");

        int bonusSocre = (int)time;
        score += bonusSocre * 20;
        scoreVictory.text = score.ToString("D7");
        if(score > PlayerPrefs.GetInt("ScoreMode" + GameManager.instance.playMode.ToString() + "Level" + GameManager.instance.levelCurrent))
        {
            PlayerPrefs.SetInt("ScoreMode" + GameManager.instance.playMode.ToString() + "Level" + GameManager.instance.levelCurrent, score);
            bestVictoryScore.text = score.ToString("D7");
        }
        else
        {
            bestVictoryScore.text = PlayerPrefs.GetInt("ScoreMode" + GameManager.instance.playMode.ToString() + "Level" + GameManager.instance.levelCurrent).ToString("D7");
        }

        restartVictoryButton.onClick.RemoveAllListeners();
        restartVictoryButton.onClick.AddListener(() => RestartGame());

        
        nextVictoryButton.onClick.RemoveAllListeners();
        nextVictoryButton.onClick.AddListener(() => NextLevel());
    }
    private void SaveStar()
    {
        if(star > PlayerPrefs.GetInt("StarMode" + GameManager.instance.playMode.ToString() + "Level" + GameManager.instance.levelCurrent))
        {
            PlayerPrefs.SetInt("StarMode" + GameManager.instance.playMode.ToString() + "Level" + GameManager.instance.levelCurrent, star);
        }
    }
    private void SaveScore()
    {

    }
    private void SetTextSupportButton()
    {
        swapText.text = swap.ToString();
        boomText.text = boom.ToString();
        searchText.text = search.ToString();
    }
    void SoundControl()
    {
        if(PlayerPrefs.GetInt("Sound") == 1)
        {
            audioSource.enabled = true;
        }
        else
        {
            audioSource.enabled = false;
        }
    }
    void MusicControl()
    {
        if (PlayerPrefs.GetInt("Music") == 1)
        {
            audioBackground.enabled = true;
        }
        else
        {
            audioBackground.enabled = false;
        }
    }
    public void AudioButton(int i)
    {
        if(i == 1)
        {
            audioSource.PlayOneShot(pressCard);
        }
        else if (i == 2)
        {
            audioSource.PlayOneShot(destroyCard);
        }
        else if(i == 3)
        {
            audioSource.PlayOneShot(pressButton);
        }
        
    }
    public void AudioSupport(int i)
    {
        if(i == 1)
        {
            audioSource.PlayOneShot(swapSupport);
        }
        else if( i == 2)
        {
            audioSource.PlayOneShot(boomSupport);
        }
        else if(i == 3)
        {
            audioSource.PlayOneShot(searchSupport);
        }
    }
    //void test(int x,int y)
    //{
    //    Vector3 vector3 = new Vector3(x, y, 0);
    //    Card card = new Card(0, 0, "Fruit1", vector3);
    //    card.CreateCard();
    //}
    private void ReadFileJson(string nameFile)
    {
        TextAsset txt = (TextAsset)Resources.Load("data/"+nameFile, typeof(TextAsset));
        string content = txt.ToString();
        itemData = JsonMapper.ToObject(content);     
        numberFruit = Int32.Parse(itemData["numberFruit"].ToString());
        gravityType = Int32.Parse(itemData["gravityType"].ToString());
        GameManager.instance.timerBurn = Int32.Parse(itemData["timerBurn"].ToString());
        //Debug.Log("So luong qua :" + numberFruit);
        for (int i = 1; i < 11; i++)
        {
            for(int j = 1; j < 9; j++)
            {
                string numb = itemData["mapData"][i - 1][j - 1].ToString();
                int interger = Int32.Parse(numb);
                matrix[i, j] = interger;
            }
        }
        //Debug.Log("Map Data dong 3: "+itemData["mapData"][2][1]);
        //Debug.Log("Map Size X: " + itemData["mapSize"]["x"]);
    }
    private void ReadFileTXT(int mode, int level, int playLevel)
    {
        string path = "";
        if (mode == 1)
        {
            path = "data/classic_mode";
        }
        else if (mode == 2)
        {
            path = "data/time_mode";
        }
        else if (mode == 3)
        {
            path = "data/night_mode";
        }
        TextAsset txt = (TextAsset)Resources.Load(path, typeof(TextAsset));
        string content = txt.ToString();       
        menuData = JsonMapper.ToObject(content);
        string nameLevel = menuData["leveldata"][level - 1][playLevel].ToString();
        totalLevelMode = menuData["leveldata"].Count;
        //Debug.Log("Tong so man trong mode" + totalLevelMode);
        GameManager.instance.totalPlayLevel = menuData["leveldata"][level - 1].Count;
        //Debug.Log("Doc file Text voi du lieu man so 1 la: " + nameLevel);
        ReadFileJson(nameLevel);

        
    }
    public void PlayMode()
    {
        int mode = GameManager.instance.playMode;
        if(mode == 3)
        {
            if (CountFruit() % 6 == 0)
            {
                FunctionController.instance.ReCreateFruit();
                FunctionController.instance.LoopFindWay();
            }
        }
        else if (mode == 2)
        {
            TimeSliderController.instance.timer +=30;
        }
        else
        {

        }
    }
    private void Load()
    {
       
        Vector3 vectorFruit, vectorButton;
        float right = -1;
        float top = -1;
        float col = 4;
        float row = 5;
        for(int i=1; i < 11; i++)
        {
            if (i < 6)
            {
                row--;
                top = 1;
            }
            else
            {
                row++;
                top = -1;
            }

            for(int j = 1; j < 9; j++)
            {
                if (j < 5)
                {
                    col--;
                    right = -1;
                }
                else
                {
                    col++;
                    right = 1;
                }
                vectorFruit = new Vector3(col * right *125 - 55, row * top * 125 - 40 + 97, 0);
                vectorButton = new Vector3(col * right * 125 - 45, row * top * 125 - 45 + 97, 0);

                int fruitID = matrix[i, j];

                if(fruitID != 0)
                {
                    string name = "Fruit" + fruitID;
                    Fruit fruit = new Fruit(i - 1, j - 1, name, vectorFruit);
                    fruit.CreateCard();

                    ButtonBG button = new ButtonBG(i - 1, j - 1, vectorButton);
                    button.CreateButton();
                }
                else
                {
                    //test(5*i, 5*j);
                }
                
                //matrix[i, j] = ran;
            }
        }
        FunctionController.instance.LoopFindWay();

    }

    public void SetMatrix()
    {
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 9; j++)
            {
                //int ran = Random.RandomRange(1, 14);
                if(matrix[i, j] != 0)
                {
                    matrix[i, j] = UnityEngine.Random.Range(2, 45);

                }
            }
        }
        for(int i=1; i < 45; i++)
        {
            if ((CountSame(i) % 2) != 0)
            {
                Change(i);
            }
        }
    }
    public void Change(int m)
    {
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 9; j++)
            {
                if (matrix[i, j] == m)
                {
                    matrix[i, j]++;
                }
            }
        }
    }
    public int CountSame(int m)
    {
        int d = 0;
        for(int i = 1; i < 11; i++)
        {
            for(int j = 1; j < 9; j++)
            {
                if (matrix[i, j] == m)
                {
                    d++;
                }
            }
        }
        return d;
    }
    public void ShowMatrix()
    {
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 9; j++)
            {
                Debug.Log(" " + matrix[i, j] + " ");
            }
            Debug.Log("Het Hang");
        }
    }
    public int CountFruit()
    {
        int c=0;
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 9; j++)
            {
                int name = matrix[i, j];
                if (name != 0)
                {
                    c++;
                }
            }
        }
                return c;
    }

    IEnumerator ShowStar(Animator anim)
    {
        yield return new WaitForSeconds(1);
        if(star == 3)
        {
            anim.SetBool("Three", true);
        }
        else if (star == 2)
        {
            anim.SetBool("Two", true);
        }
        else
        {
            anim.SetBool("One", true);
        }
    }

}
