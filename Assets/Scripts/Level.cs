﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level : MonoBehaviour {

    public Transform transform;
    public GameObject[,] level = new GameObject[5, 4];

    public int x;
    public int y;
    public Vector3 vector3;
    public int number;
    public string namePage;

    public Level(int x, int y, Vector3 vector3, int number, string namePage)
    {
        this.x = x;
        this.y = y;
        this.vector3 = vector3;
        this.number = number;
        this.namePage = namePage;
    }

    public void CreateButton()
    {
        transform = GameObject.Find(namePage).GetComponent<Transform>();
        level[x, y] = Instantiate(Resources.Load("Level"), vector3, Quaternion.identity) as GameObject;
        level[x, y].transform.SetParent(transform, false);

        //set number level
        level[x, y].name = level[x, y].GetInstanceID().ToString();
        GameObject levelObject = GameObject.Find(level[x, y].name);
        levelObject.transform.GetChild(2).transform.GetComponent<Text>().text = number.ToString();
        int star = PlayerPrefs.GetInt("StarMode" + GameManager.instance.playMode.ToString() + "Level" + number.ToString());
        if(star == 0)
        {
            levelObject.transform.GetChild(1).gameObject.SetActive(true);
            levelObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        else if(star == 1)
        {
            levelObject.transform.GetChild(1).gameObject.SetActive(true);
            levelObject.transform.GetChild(3).gameObject.SetActive(true);
            levelObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        else if(star == 2)
        {
            levelObject.transform.GetChild(1).gameObject.SetActive(true);
            levelObject.transform.GetChild(4).gameObject.SetActive(true);
            levelObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        else if(star == 3)
        {
            levelObject.transform.GetChild(1).gameObject.SetActive(true);
            levelObject.transform.GetChild(5).gameObject.SetActive(true);
            levelObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        else
        {
            levelObject.transform.GetChild(0).gameObject.SetActive(true);
        }
        PlayerPrefs.SetInt(level[x, y].name, number);
        if (number == 1 && (PlayerPrefs.GetInt("StarMode" + GameManager.instance.playMode.ToString() + "Level1") < 1))
        {
            levelObject.transform.GetChild(1).gameObject.SetActive(true);
            levelObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        if(PlayerPrefs.GetInt("StarMode" + GameManager.instance.playMode.ToString() + "Level" + (number - 1).ToString()) > 0 &&
            PlayerPrefs.GetInt("StarMode" + GameManager.instance.playMode.ToString() + "Level" + number.ToString()) < 1)
        {
            levelObject.transform.GetChild(1).gameObject.SetActive(true);
            levelObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        //PlayerPrefs.Save();
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
