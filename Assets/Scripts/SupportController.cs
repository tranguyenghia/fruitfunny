﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SupportController : MonoBehaviour
{
    public static SupportController instance;
    public GameObject boomButton, boomBlur, swapBlur, searchBlur;
    // Use this for initialization
    void Start()
    {
        MakeInstance();
        PlayerPrefs.SetInt("Boom", 0);
        PlayerPrefs.Save();
    }
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void Test()
    {
        FunctionController.instance.GetMatrix();
    }
    public void Boom()
    {
        OffPressEffect();
        if (GamePlayController.instance.boom > 0 && PlayerPrefs.GetInt("Boom") == 0)
        {
            SetScaleBoomButton(2);
            PlayerPrefs.SetInt("Boom", 1);
            PlayerPrefs.Save();
            GamePlayController.instance.boom--;
            if(GamePlayController.instance.boom == 0)
            {
                boomBlur.SetActive(true);
            }
        }
        else
        {
            
        }
        
        
    }
    public void Search()
    {
        OffPressEffect();
        if (FunctionController.instance.searchNext && GamePlayController.instance.search > 0 && PlayerPrefs.GetInt("Boom") == 0)
        {
            PlayerPrefs.SetInt("Press", 0);
            GamePlayController.instance.AudioSupport(3);
            FunctionController.instance.Search();
            GamePlayController.instance.search--;
            if (GamePlayController.instance.search == 0)
            {
                searchBlur.SetActive(true);
            }
        }
        else
        {
            
        }
        
    }
    public void Swap()
    {
        OffPressEffect();
        if (GamePlayController.instance.swap > 0 && PlayerPrefs.GetInt("Boom") == 0)
        {
            GamePlayController.instance.AudioSupport(1);
            FunctionController.instance.ReCreateFruit();
            FunctionController.instance.LoopFindWay();
            GamePlayController.instance.swap--;
            if (GamePlayController.instance.swap == 0)
            {
                swapBlur.SetActive(true);
            }
        }
        else
        {
            
        }
        
    }
    public void SetScaleBoomButton(int i)
    {
        Vector3 scale = boomButton.transform.localScale;
        if (i == 2)
        {
            scale.x = 1.5f;
            scale.y = 1.5f;
        }
        else if(i == 1)
        {
            scale.x = 1f;
            scale.y = 1f;
        }
        boomButton.transform.localScale = scale;
    }
    
    private void OffPressEffect()
    {
        int x = GamePlayController.instance.xFruitSelected;
        int y = GamePlayController.instance.yFruitSelected;
        if (x!= 0)
        {
            GameObject button = GameObject.Find(GamePlayController.instance.matrixButtonBGObject[x, y]);
            button.transform.GetChild(0).gameObject.SetActive(true);
            button.transform.GetChild(2).gameObject.SetActive(false);

            GamePlayController.instance.xFruitSelected = 0;
            GamePlayController.instance.yFruitSelected = 0;
        }
    }
    //public void OnPointerDown(PointerEventData data)
    //{
    //    Vector3 scale = this.gameObject.transform.localScale;
    //    scale.x = 2f;
    //    scale.y = 2f;
    //    this.gameObject.transform.localScale = scale;
    //    if(gameObject.name == "Boom")
    //    {
    //        PlayerPrefs.SetInt("Boom", 1);
    //        PlayerPrefs.Save();
    //    }
    //    else if(gameObject.name == "Search")
    //    {
    //        //FunctionController.instance.FindWay();
    //        FunctionController.instance.Search();
    //    }
    //    else if (gameObject.name == "GravityH")
    //    {
            
    //        FunctionController.instance.GetMatrix();
    //    }
    //    else
    //    {
    //        FunctionController.instance.ReCreateFruit();
    //    }
    //}
    //public void OnPointerUp(PointerEventData data)
    //{
    //}
}