﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateButtonController : MonoBehaviour {

    public GameObject[,] instance = new GameObject[10, 8];

    public Transform canvas;



    // Use this for initialization
    void Start()
    {

        Load();

    }

    // Update is called once per frame
    void Update()
    {

    }


    private void Load()
    {
        Vector3 vector3;
        float right = -1;
        float top = -1;
        float col = 4;
        float row = 5;
        for (int i = 1; i < 11; i++)
        {
            if (i < 6)
            {
                row--;
                top = 1;
            }
            else
            {
                row++;
                top = -1;
            }

            for (int j = 1; j < 9; j++)
            {
                if (j < 5)
                {
                    col--;
                    right = -1;

                }
                else
                {
                    col++;
                    right = 1;
                }
                vector3 = new Vector3(col * right * 125 - 45, row * top * 125 - 45 + 97, 0);
                int x = i - 1;
                int y = j - 1;
                //instance[i, j] = Instantiate(Resources.Load("Fruit " + "(" + ran + ")"), vector3, Quaternion.identity) as GameObject;
                instance[x, y] = Instantiate(Resources.Load("Fruit"), vector3, Quaternion.identity) as GameObject;
                instance[x, y].transform.SetParent(canvas, false);
                instance[x, y].name = instance[x, y].GetInstanceID().ToString();
                PlayerPrefs.SetString(x.ToString() + "." + y.ToString(), instance[x, y].name);
                PlayerPrefs.Save();
            }
        }

    }
}
