﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageLevel : MonoBehaviour {

    public Transform transform;
    public GameObject[] page = new GameObject[50];

    public int id;
    public Vector3 vector3;

    public PageLevel(int id, Vector3 vector3)
    {
        this.id = id;
        this.vector3 = vector3;
    }

    public void CreatePage()
    {
        transform = GameObject.FindGameObjectWithTag("ContentLevel").GetComponent<Transform>();
        page[id] = Instantiate(Resources.Load("Page"), vector3, Quaternion.identity) as GameObject;
        page[id].transform.SetParent(transform, false);

        page[id].name = page[id].GetInstanceID().ToString();
        PlayerPrefs.SetString("Page" + id.ToString(), page[id].name);
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
