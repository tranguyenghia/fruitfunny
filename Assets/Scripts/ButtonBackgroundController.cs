﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBackgroundController : MonoBehaviour {

    public static ButtonBackgroundController instance;
    public GameObject buttonLight, buttonDark, buttonChose;
    // Use this for initialization
    void Start()
    {
        MakeInstance();
    }
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PressButton()
    {
        if (!buttonChose.activeSelf)
        {
            buttonChose.SetActive(true);
            buttonLight.SetActive(false);
        }
    }
    public void CancelPress()
    {
        if (buttonChose.activeSelf)
        {
            buttonChose.SetActive(false);
            buttonLight.SetActive(true);
        }
    }


    
}

