﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScript : MonoBehaviour
{

    // Use this for initialization


    GameObject gos;
    float stepX = 0.42159564f;
    float stepScale = 0.1747888f;
    float startX, startScale;
    Vector3 position, scale;

    float tast = 0.1f;
    void Start()
    {
        //gos = GameObject.FindGameObjectWithTag("progressbar");
        //position = gos.transform.position;
        //scale = gos.transform.localScale;
        //startX = position.x;
        //startScale = scale.x;
        StartCoroutine(LoadNewScene());
    }
    // Updates once per frame
    void Update()
    {
        //		tast +=0.1f;
        //		gos.transform.position = new Vector3 (startX + stepX * tast * 100, position.y, position.z);
        //		gos.transform.localScale = new Vector3 (startScale - stepScale * tast * 100, scale.y, scale.z);
    }


    // The coroutine runs on its own at the same time as Update() and takes an integer indicating which scene to load.
    IEnumerator LoadNewScene()
    {

        //yield return new WaitForSeconds(1);
        // Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
        AsyncOperation async = Application.LoadLevelAsync("MainMenu");


        // While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
        while (!async.isDone)
        {
            Debug.Log("xxx " + startX + stepX * async.progress * 10);
            //Debug.Log("xxx " + startScale + stepScale * async.progress * 100);
            //gos.transform.position = new Vector3(startX + stepX * async.progress * 100, position.y, position.z);
            //gos.transform.localScale = new Vector3(startScale - stepScale * async.progress * 100, scale.y, scale.z);
            yield return null;
        }

    }
}
