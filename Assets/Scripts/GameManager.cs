﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System;

public class GameManager : MonoBehaviour {
    public static GameManager instance;

    public int music = 1, sound = 1;
    private const string MUSIC = "Music", SOUND = "Sound";

    private string jsonString, txtString;
    public JsonData menuData, itemData;

    int[,] matrix = new int[11, 9];
    int numberFruit;

    public int playMode = 0;
    public int levelCurrent = 1;
    public int timerBurn = 3;
    public int numberPlayLevel = 0, totalPlayLevel = 0;
    

    private void Awake()
    {
        //PlayerPrefs.DeleteAll();
        MakeSingleInstance();
        IsGameStartedForTheFirstTime();

        music = GetMusic();
        sound = GetSound();
        //getStar();
        
    }
    
    void MakeSingleInstance()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
        //SetMusic(music);
        //SetSound(sound);
        //Debug.Log("Tong so play trong level 1 la: " + totalPlayLevel);
    }
    private JsonData ReadFileJson(string nameFile)
    {
        TextAsset txt = (TextAsset)Resources.Load("data/" + nameFile, typeof(TextAsset));
        string content = txt.ToString();
        JsonData itemData = JsonMapper.ToObject(content);
        return itemData;        
    }

    public JsonData ReadFileTXT(string nameFile)
    {
        TextAsset txt = (TextAsset)Resources.Load(nameFile, typeof(TextAsset));
        string content = txt.ToString();
        JsonData menuData = JsonMapper.ToObject(content);
        return menuData;
    }

    
    public void PlayMode(string nameFileTXT)
    {
        menuData = ReadFileTXT(nameFileTXT);
        switch (nameFileTXT)
        {
            case "data/classic_mode":
                playMode = 1;
                break;
            case "data/time_mode":
                playMode = 2;
                break;
            case "data/night_mode":
                playMode = 3;
                break;
        }
    }
    public void getStar()
    {
        int levelClassic = ReadFileTXT("data/classic_mode")["leveldata"].Count;
        for (int i = 1; i < levelClassic + 1; i++)
        {
            int numb = PlayerPrefs.GetInt("StarLevel" + i.ToString());
            Debug.Log("Star cua level " + i + " la: " + numb);
        }
    }
    void IsGameStartedForTheFirstTime()
    {
        if (!PlayerPrefs.HasKey("IsGameStartedForTheFirstTime"))
        {
            PlayerPrefs.SetInt(MUSIC, 1);
            PlayerPrefs.SetInt(SOUND, 1);
            int levelClassic = ReadFileTXT("data/classic_mode")["leveldata"].Count;
            SetStarAndScoreForTheFirstTime(levelClassic, 1);
            int levelTime = ReadFileTXT("data/time_mode")["leveldata"].Count;
            SetStarAndScoreForTheFirstTime(levelTime, 2);
            int levelNight = ReadFileTXT("data/night_mode")["leveldata"].Count;
            SetStarAndScoreForTheFirstTime(levelNight, 3);
            //for(int i = 1; i < levelClassic + 1; i++)
            //{
            //    PlayerPrefs.SetInt("StarLevel" + i.ToString(), -1);
            //}
            PlayerPrefs.SetInt("IsGameStartedForTheFirstTime", 0);
        }
    }
    // get set backgroud music
    private void SetStarAndScoreForTheFirstTime(int amount, int mode)
    {
        for (int i = 1; i < amount + 1; i++)
        {
            PlayerPrefs.SetInt("StarMode" + mode.ToString() + "Level" + i.ToString(), -1);
            PlayerPrefs.SetInt("ScoreMode" + mode.ToString() + "Level" + i.ToString(), 0);
        }
    }
   
    public void SetMusic(int music)
    {
        PlayerPrefs.SetInt(MUSIC, music);
    }
    public int GetMusic()
    {
        return PlayerPrefs.GetInt(MUSIC);
    }
    // get set sound effect
    public void SetSound(int sound)
    {
        PlayerPrefs.SetInt(SOUND, sound);
    }
    public int GetSound()
    {
        return PlayerPrefs.GetInt(SOUND);
    }
}
