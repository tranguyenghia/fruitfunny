﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeSliderController : MonoBehaviour {
    public static TimeSliderController instance;

    private Slider slider;
    public float timer = 100f;
    public float timerBurn = 1f;
    int timerBurnFromFile;
    public GameObject warring;
	// Use this for initialization
	void Start () {
        GetPrefereces();
        MakeInstance();
        int mode = GameManager.instance.playMode;
        //if(mode == 1)
        //{
        //    timerBurn = 1.6f;
        //}
        //else if (mode == 2)
        //{
        //    timerBurn = 10f;
        //}
        //else
        //{
        //    timerBurn = 1.3f;
        //}
        timerBurnFromFile = GameManager.instance.timerBurn;
        if(mode == 3)
        {
            timerBurn = timerBurnFromFile * 0.74f;
        }
        else if(mode == 2)
        {
            timerBurn = timerBurnFromFile * 0.16f;
        }
        else
        {
            timerBurn = timerBurnFromFile;
        }
        //Debug.Log("Thoi gian chay: " + timerBurn);
	}
	void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
	// Update is called once per frame
	void Update () {
        if (timer > 0 && !GamePlayController.instance.victory)
        {
            if (timer > 100)
            {
                timer = 100f;
            }
            timer -= (5 / timerBurn) * UnityEngine.Time.deltaTime;
            slider.value = timer;
        }
        //show, hide effect warring
        if (timer < 16)
        {
            warring.SetActive(true);
        }
        else
        {
            warring.SetActive(false);
        }

        if (GamePlayController.instance.victory || GamePlayController.instance.fail)
        {
            warring.SetActive(false);
        }
    }
    
    void GetPrefereces()
    {
        slider = GameObject.Find("Slider").GetComponent<Slider>();

        slider.minValue = 0f;
        slider.maxValue = timer;
        slider.value = slider.maxValue;
    }
}
