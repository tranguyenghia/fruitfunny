﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

    private JsonData menuData;
    public GameObject soundOnButton, soundMuteButton, musicOnButton, musicMuteButton;

    public Image white;
    public Animator anim;

    public AudioSource audioMusic;
    public AudioSource audioSound;
    public AudioClip pressButton;
    // Use this for initialization
    void Start () {
        StartCoroutine(FadeIn());
        SoundControl();
        MusicControl();
        UnityEngine.Time.timeScale = 1f;
        SetButtonMusic();
        SetButtonSound();

    }
    void SoundControl()
    {
        if (PlayerPrefs.GetInt("Sound") == 1)
        {
            audioSound.enabled = true;
        }
        else
        {
            audioSound.enabled = false;
        }
    }
    void MusicControl()
    {
        if (PlayerPrefs.GetInt("Music") == 1)
        {
            audioMusic.enabled = true;
        }
        else
        {
            audioMusic.enabled = false;
        }
    }
    // Update is called once per frame
    void Update () {
        //SetButtonMusic();
        //SetButtonSound();
    }
    public void SetOnMusic()
    {
        PlayerPrefs.SetInt("Music", 1);
        SetButtonMusic();
        MusicControl();
    }
    public void SetOnSound()
    {
        PlayerPrefs.SetInt("Sound", 1);
        SetButtonSound();
        SoundControl();
    }
    public void SetMuteMusic()
    {
        PlayerPrefs.SetInt("Music", 0);
        SetButtonMusic();
        MusicControl();
    }
    public void SetMuteSound()
    {
        PlayerPrefs.SetInt("Sound", 0);
        SetButtonSound();
        SoundControl();
    }
    private void SetButtonSound()
    {
        if(PlayerPrefs.GetInt("Sound") == 1)
        {
            soundOnButton.SetActive(true);
            soundMuteButton.SetActive(false);
        }
        else
        {
            soundOnButton.SetActive(false);
            soundMuteButton.SetActive(true);
        }
    }
    private void SetButtonMusic()
    {
        if(PlayerPrefs.GetInt("Music") == 1)
        {
            musicOnButton.SetActive(true);
            musicMuteButton.SetActive(false);
        }
        else
        {
            musicMuteButton.SetActive(true);
            musicOnButton.SetActive(false);
        }
    }
    public void PressClasic()
    {
        white.gameObject.SetActive(true);
        GameManager.instance.PlayMode("data/classic_mode");
        audioSound.PlayOneShot(pressButton);
        StartCoroutine(Fading());
        //Application.LoadLevel("LevelMenu");
        //int count = GameManager.instance.menuData["leveldata"].Count;
        //Debug.Log("Dem so phan tu khi chuyen readfile qua GameManager: " + count);
    }
    public void PressTime()
    {
        white.gameObject.SetActive(true);
        audioSound.PlayOneShot(pressButton);
        GameManager.instance.PlayMode("data/time_mode");
        StartCoroutine(Fading());
    }
    public void PressNight()
    {
        white.gameObject.SetActive(true);
        audioSound.PlayOneShot(pressButton);
        GameManager.instance.PlayMode("data/night_mode");
        StartCoroutine(Fading());
    }
    private JsonData ReadFileTXT(string nameFile)
    {
        TextAsset txt = (TextAsset)Resources.Load(nameFile, typeof(TextAsset));
        string content = txt.ToString();
        menuData = JsonMapper.ToObject(content);
        string nameLevel = menuData["leveldata"][0][0].ToString();
        //Debug.Log("Doc file Text voi du lieu man so 1 la: " + nameLevel);
        //ReadFileJson(nameLevel);
        int count = menuData["leveldata"].Count;
        //Debug.Log("so phan tu cua classic mode la: " + count);
        return menuData;
    }

    IEnumerator Fading()
    {
        
        anim.SetBool("Fade", true);
        yield return new WaitUntil(() => white.color.a == 1);
        SceneManager.LoadScene("LevelMenu");
    }

    IEnumerator FadeIn()
    {
        yield return new WaitUntil(() => white.color.a == 0);
        white.gameObject.SetActive(false);
    }
}
