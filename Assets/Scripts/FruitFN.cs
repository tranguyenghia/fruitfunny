﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FruitFN : MonoBehaviour {
    public static FruitFN instance;

    public Transform canvas;


    public int[,] matrix; //This matrix includes borders
    // Use this for initialization
    private int x;
    private int y;
    private int name;

    public FruitFN fruit1 = null, fruit2 = null;
    public bool createMatrix = false, flag = true;
    void Start()
    {
        matrix = new int[12, 10];
        //GetMatrix();
        MakeInstance();

    }
    void Update()
    {
        //Debug.Log("CreateMatrix"+createMatrix);
    }
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    public FruitFN(int x, int y, int name)
    {
        this.X = x;
        this.Y = y;
        this.Name = name;

    }
    public int X
    {
        get
        {
            return x;
        }

        set
        {
            x = value;
        }
    }

    public int Y
    {
        get
        {
            return y;
        }

        set
        {
            y = value;
        }
    }

    public int Name
    {
        get
        {
            return name;
        }

        set
        {
            name = value;
        }
    }

    public void setFruit1(FruitFN fruit)
    {
        this.fruit1 = fruit;
        
    }
    public void setFruit2(FruitFN fruit)
    {
        this.fruit2 = fruit;
        
    }
    public FruitFN getFruit1()
    {
        return fruit1;
    }
    public FruitFN getFruit2()
    {
        return fruit2;
    }
    private bool checkTheSameFruit()
    {
        if (fruit1.X == fruit2.X && fruit1.Y == fruit2.Y)
        {
            return true;
        }
        return false;
    }
    public int CheckTwoFruit()
    {
        GetMatrix();
        //showMatrix();
        //Debug.Log("X cua qua 1: " + fruit1.X);
        // Debug.Log("X cua qua 2: " + fruit2.X);
        if (fruit1.Name == fruit2.Name)
        {
            if (fruit1.X == fruit2.X)
            {

                if (checkLineX(fruit1.Y, fruit2.Y, fruit1.X) != -1 && !checkTheSameFruit())
                {

                    return checkLineX(fruit1.Y, fruit2.Y, fruit1.X);
                }
            }
            if (fruit1.Y == fruit2.Y)
            {
                if (checkLineY(fruit1.X, fruit2.X, fruit1.Y) != -1 && !checkTheSameFruit())
                {

                    return 20 + checkLineY(fruit1.X, fruit2.X, fruit1.Y);
                }
            }
            // check retangle with x
            if (checkRectangleX(fruit1, fruit2) != -1 && !checkTheSameFruit())
            {

                return 40 + checkRectangleX(fruit1, fruit2);
            }
            // check retangle with y
            if (checkRectangleY(fruit1, fruit2) != -1 && !checkTheSameFruit())
            {

                return 60 + checkRectangleY(fruit1, fruit2);
            }
            //check more right
            if (checkMoreLineX(fruit1, fruit2, 1) != -1 && !checkTheSameFruit())
            {

                return 80 + checkMoreLineX(fruit1, fruit2, 1);
            }
            //check more left
            if (checkMoreLineX(fruit1, fruit2, -1) != -1 && !checkTheSameFruit())
            {

                return 100 + checkMoreLineX(fruit1, fruit2, -1);
            }
            //check more up
            if (checkMoreLineY(fruit1, fruit2, -1) != -1 && !checkTheSameFruit())
            {

                return 120 + checkMoreLineY(fruit1, fruit2, -1);
            }
            //check more down
            if (checkMoreLineY(fruit1, fruit2, 1) != -1 && !checkTheSameFruit())
            {

                return 140 + checkMoreLineY(fruit1, fruit2, 1);
            }
        }
        
        
        return -1;  
        
    }

    public int checkLineX(int y1, int y2, int x)
    {
        int min = Math.Min(y1, y2);
        int max = Math.Max(y1, y2);
        int check = -1;
        //Debug.Log("Min la: " + min + "Max la: " + max);
        //run column
        if (max - min == 1)
        {
            return 1;
        }
        for (int y = min+1; y < max; y++)
        {
            //Debug.Log("Qua nhan la: " + fruit1.Name);
            // Debug.Log("Duyet den: "+matrix[x, y]);
            //if (fruit1.Name == matrix[x, y] || matrix[x, y] == "Null")
            //{
            //    check = max-min;
            //}
            //else
            //{
            //    check = -1;
            //    break;
            //}

            if (matrix[x, y] == 0)
            {
                check = max - min;
            }
            else
            {
                check = -1;
                break;
            }

        }

        return check;
    }
    public int checkLineY(int x1, int x2, int y)
    {
        int min = Math.Min(x1, x2);
        int max = Math.Max(x1, x2);
        int check = -1;
        if (max - min == 1)
        {
            return 1;
        }
        for(int x = min+1; x < max; x++)
        {
            //if (fruit1.Name == matrix[x, y] || matrix[x, y] == "Null")
            //{
            //    check = max-min;
            //}
            //else
            //{
            //    check = -1;
            //    break;
            //}
            if(matrix[x, y] == 0)
            {
                check = max - min;
            }
            else
            {
                check = -1;
                break;
            }
        }
        return check;
    }
    private  int checkRectangleX(FruitFN fruit1, FruitFN fruit2)
    {
        //find card have y min, max;
        FruitFN fMinY = fruit1, fMaxY = fruit2;
        if(fruit1.Y > fruit2.Y)
        {
            fMinY = fruit2;
            fMaxY = fruit1;
        }

        for (int y = fMinY.Y; y <= fMaxY.Y; y++)
        {
            if (y == fMinY.Y)
            {
                if(matrix[fMaxY.X, y] == 0)
                {
                    if (checkLineY(fMinY.X, fMaxY.X, y) != -1
                        && checkLineX(y, fMaxY.Y, fMaxY.X) != -1)
                    {

                        return y;
                    }
                }
                
            }
            else if (y == fMaxY.Y)
            {
                if (matrix[fMinY.X, y] == 0)
                {
                    if (checkLineX(fMinY.Y, y, fMinY.X) != -1
                        && checkLineY(fMinY.X, fMaxY.X, y) != -1)
                    {
                        return y;
                    }
                }
                
            }
            else
            {
                if (matrix[fruit1.X, y] == 0 && matrix[fruit2.X, y] == 0)
                {
                    if (checkLineX(fMinY.Y, y, fMinY.X) != -1
                        && checkLineY(fMinY.X, fMaxY.X, y) != -1
                        && checkLineX(y, fMaxY.Y, fMaxY.X) != -1)
                    {
                        return y;
                    }

                }
            }

        }


        return -1;
    }
    private int checkRectangleY(FruitFN fruit1, FruitFN fruit2)
    {
        // find card have x min , max
        FruitFN fMinX = fruit1, fMaxX = fruit2;
        if(fruit1.X > fruit2.X)
        {
            fMinX = fruit2;
            fMaxX = fruit1;
        }
        for (int x = fMinX.X; x <= fMaxX.X; x++)
        {
            if (x == fMinX.X)
            {
                if(matrix[x, fMaxX.Y] == 0)
                {
                    if (checkLineX(fMinX.Y, fMaxX.Y, x) != -1
                    && checkLineY(x, fMaxX.X, fMaxX.Y) != -1)
                    {
                        return x;
                    }
                }
                
            }
            else if (x == fMaxX.X)
            {
                if(matrix[x, fMinX.Y] == 0)
                {
                    if (checkLineY(fMinX.X, x, fMinX.Y) != -1
                    && checkLineX(fMinX.Y, fMaxX.Y, x) != -1)
                    {
                        return x;
                    }
                }
                
            }
            else
            {
                if (matrix[x, fruit1.Y] == 0 && matrix[x, fruit2.Y] == 0)
                {
                    if (checkLineY(fMinX.X, x, fMinX.Y) != -1
                    && checkLineX(fMinX.Y, fMaxX.Y, x) != -1
                    && checkLineY(x, fMaxX.X, fMaxX.Y) != -1)
                    {
                        return x;
                    }
                }
                
            } 
        }


        return -1;
    }
    private int checkMoreLineX(FruitFN fruit1, FruitFN fruit2, int type)
    {
        // find fMaxY, fMinY in Fruit
        int check = -1;
        FruitFN fMinY = fruit1, fMaxY = fruit2;
        if (fruit1.Y > fruit2.Y)
        {
            fMinY = fruit2;
            fMaxY = fruit1;
        }
        int y = 0;
        if (type == 1)
        {
            //check more RIGHT
            
            y = fMaxY.Y+1;
            //Debug.Log("Check more RIGHT voi y = " + y);
        }
        else
        {
            //check more LEFT
            y = fMinY.Y-1;
            //Debug.Log("Check more LEFT voi y = " + y);

        }    
        while (checkLineX(fMinY.Y, y, fMinY.X) != -1)
        {
            if (matrix[fruit1.X, y] == 0 && matrix[fruit2.X, y] == 0)
            {
                //Debug.Log("thanh cong duong min - y");
                if (checkLineY(fMinY.X, fMaxY.X, y) != -1)
                {
                    //Debug.Log("thanh cong duong y");
                    if (checkLineX(fMaxY.Y, y, fMaxY.X) != -1)
                    {
                        //Debug.Log("thanh cong duong max - y voi gia tri duong y là: " +y);
                        //Debug.Log("Check the same Fruit " + checkTheSameFruit());
                        return y;
                    }
                }
            }
            
            
            y += type;
            if (y > 9 || y < 0)
            {
                return -1;
                break;
            }

        }
        return -1;
    }
    private int checkMoreLineY(FruitFN fruit1, FruitFN fruit2, int type)
    {
        int check = -1;
        FruitFN fMinX = fruit1, fMaxX = fruit2;
        if(fruit1.X > fruit2.X)
        {
            fMinX = fruit2;
            fMaxX = fruit1;
        }
        int x = 0;
        if(type == 1)
        {
            //check more DOWN
            x = fMaxX.X+1;
        }
        else
        {
            //check more UP
            x = fMinX.X-1;
        }
        while(checkLineY(x, fMinX.X, fMinX.Y) != -1)
        {
            if(matrix[x, fruit1.Y] == 0 && matrix[x, fruit2.Y] == 0)
            {
                if (checkLineX(fMinX.Y, fMaxX.Y, x) != -1)
                {
                    if (checkLineY(fMaxX.X, x, fMaxX.Y) != -1)
                    {
                        return x;
                    }
                }
            }
            
            x += type;
            if(x>11 || x < 0)
            {
                return -1;
                break;
            }
        }
        return -1;
    }
    private void GetMatrix()
    {
 
        for (int i = 0; i < 12; i++)
        {
            matrix[i, 0] = 0;
            matrix[i, 9] = 0;
            for (int j = 0; j < 10; j++)
            {
                matrix[0, j] = 0;
                matrix[11, j] = 0;
            }
        }
        for(int m = 1; m < 11; m++)
        {
            for(int n = 1; n < 9; n++)
            {
                int name = GamePlayController.instance.matrix[m, n];
                if(name == 0)
                {
                    matrix[m, n] = 0;
                }
                else
                {
                    matrix[m, n] =  name;
                }
                
            }
        }
    }
    private void showMatrix()
    {
        for(int i=0; i < 12; i++)
        {
            for(int j = 0; j < 10; j++)
            {
                Debug.Log(matrix[i, j]);
            }
            Debug.Log("Het Hang");
        }
    }
    

    
	
	// Update is called once per frame
	
}
