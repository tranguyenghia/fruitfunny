﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingController : MonoBehaviour {

    public Text textLevel;
	// Use this for initialization
	void Start () {
        UnityEngine.Time.timeScale = 1;
        textLevel.text = "LEVEL " + GameManager.instance.levelCurrent;
        StartCoroutine(PlayGame());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    IEnumerator PlayGame()
    {
        yield return new WaitForSeconds(0.2f);
        Application.LoadLevel("GamePlay");
    }
}
