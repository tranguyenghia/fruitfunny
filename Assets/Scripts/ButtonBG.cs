﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBG : MonoBehaviour {
    public Transform canvas;
    public GameObject[,] instance = new GameObject[10, 8];
    public int x;
    public int y;
    public Vector3 vector3;

    public ButtonBG(int x, int y, Vector3 vector3)
    {
        this.x = x;
        this.y = y;
        this.vector3 = vector3;
    }

    public void CreateButton()
    {
        canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Transform>();

        instance[x, y] = Instantiate(Resources.Load("Fruit"), vector3, Quaternion.identity) as GameObject;
        instance[x, y].transform.SetParent(canvas, false);
        instance[x, y].name = instance[x, y].GetInstanceID().ToString();
       
        GamePlayController.instance.matrixButtonBGObject[x + 1, y + 1] = instance[x, y].name;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
