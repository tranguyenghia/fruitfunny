﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMenuController : MonoBehaviour {

    public static LevelMenuController instance;
    public GameObject titleClassic, titleTime, titleNight;
    int countLevel;

    public Image white;
    public Animator anim;

    public AudioSource audioSound;
    public AudioClip pressButton;
    // Use this for initialization
    void Start () {
        StartCoroutine(FadeIn());
        SoundControl();
        //namePage = new string[50];
        SetTitle();
        Load(GameManager.instance.playMode);
        MakeInstance();
	}
	void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Escape))
        {
            BackToMenu();
        }
	}
    void SoundControl()
    {
        if (PlayerPrefs.GetInt("Sound") == 1)
        {
            audioSound.enabled = true;
        }
        else
        {
            audioSound.enabled = false;
        }
    }
    public void BackToMenu()
    {
        audioSound.PlayOneShot(pressButton);
        Application.LoadLevel("MainMenu");
    }
    void SetTitle()
    {
        int mode = GameManager.instance.playMode;
        switch (mode)
        {
            case 1:
                titleClassic.SetActive(true);
                break;
            case 2:
                titleTime.SetActive(true);
                break;
            case 3:
                titleNight.SetActive(true);
                break;
        }
    }
    public void LoadGamePlay()
    {
        white.gameObject.gameObject.SetActive(true);
        Debug.Log("Load GamePlay");
        Application.LoadLevel("GamePlay");
    }

    private void LoadPage(string namePage, int numberPlus)
    {
        Vector3 vectorLevel;
        float right = -1;
        float top = -1;
        float col = 2;
        float row = 3;
        for (int i = 1; i < 6; i++)
        {
            if (i < 4)
            {
                row--;
                top = 1;
            }
            else
            {
                row++;
                top = -1;
            }

            for (int j = 1; j < 5; j++)
            {
                if (j < 3)
                {
                    col--;
                    right = -1;
                }
                else
                {
                    col++;
                    right = 1;
                }
                int number = numberPlus + (i - 1) * 4 + j;
                if(number <= countLevel)
                {
                    vectorLevel = new Vector3(col * right * 230 - 100, row * top * 260 - 100 + 150, 0);
                    Level level = new Level(i - 1, j - 1, vectorLevel, number, namePage);
                    level.CreateButton();
                }
                else
                {

                }
                

            }
        }
    }
    private void Load(int mode)
    {
        string path = "";
        if (mode == 1)
        {
            path = "data/classic_mode";
        }
        else if (mode == 2)
        {
            path = "data/time_mode";
        }
        else if (mode == 3)
        {
            path = "data/night_mode";
        }
        Vector3 vector3 = new Vector3();
        countLevel = GameManager.instance.ReadFileTXT(path)["leveldata"].Count;
        //if (countLevel > 0)
        //{
        //    LoadPage("Page1", 0);
        //    if (countLevel > 20)
        //    {
        //        LoadPage("Page2", 20);
        //        if(countLevel > 40)
        //        {
        //            LoadPage("Page3", 40);
        //            if (countLevel > 60)
        //            {
        //                LoadPage("Page4", 60);
        //                if (countLevel > 80)
        //                {
        //                    LoadPage("Page5", 80);
        //                }
        //            }
        //        }
        //    }
        //}
        int numberPage = countLevel / 20 + 1;
        //Debug.Log("so luong page can tao: " + numberPage);
        for(int i = 0; i < numberPage; i++)
        {
            PageLevel pageLevel = new PageLevel(i, vector3);
            pageLevel.CreatePage();
            LoadPage(PlayerPrefs.GetString("Page" + i), i * 20);

        }

    }
    public IEnumerator Fading()
    {
        white.gameObject.SetActive(true);
        audioSound.PlayOneShot(pressButton);
        anim.SetBool("Fade", true);
        yield return new WaitUntil(() => white.color.a == 1);
        SceneManager.LoadScene("GamePlay");
    }
    IEnumerator FadeIn()
    {
        yield return new WaitUntil(() => white.color.a == 0);
        white.gameObject.SetActive(false);
    }
}
