﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineController : MonoBehaviour {
    public static LineController instance;
    
    
    public Transform canvas, canvas1;
    Vector3 vector3;
    public GameObject inst, inst1;
    int[,] matrixX = new int[12, 10];
    int[,] matrixY = new int[12, 10];

    // Use this for initialization
    void Start () {
        MakeInstance();
        CreateMatrixX();
        CreateMatrixY();

	}
	void MakeInstance()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
	// Update is called once per frame
	void Update () {
       

    }
    private void CreatePoint(int x, int y)
    {
        float xLine = matrixX[x, y];
        float yLine = matrixY[x, y];
        vector3 = new Vector3(xLine, yLine, 0);
        //create star
        inst = Instantiate(Resources.Load("Point"), vector3, Quaternion.identity) as GameObject;
        inst.transform.SetParent(canvas, false);
        //create point
        inst1 = Instantiate(Resources.Load("Point1"), vector3, Quaternion.identity) as GameObject;
        inst1.transform.SetParent(canvas1, false);
    }
    public void CreateVertical(int x1, int x2, int y)
    {
        //Debug.Log("Create Vertical X1: " + x1 + " X2: " + x2 + " y: " + y);
        float xLine = matrixX[x1, y] + matrixX[x2, y];
        float yLine = matrixY[x1, y] + matrixY[x2, y];
        
        //Debug.Log("Gay X: " + x + "Gay Y: " + y);
        vector3 = new Vector3(xLine / 2, yLine / 2, 0);
        Vector3 scale = transform.localScale;
        inst = Instantiate(Resources.Load("Vertical"), vector3, Quaternion.identity) as GameObject;
        inst.transform.SetParent(canvas, false);
        scale.y = (x1 - x2) * 1.2f;

        inst.transform.localScale = scale;
        
        
    }
    public void CreateHorizontal(int y1, int y2, int x)
    {
        //Debug.Log("Create Vertical Y1: " + y1 + " Y2: " + y2 + " X: " + x);
        float xLine = matrixX[x, y1] + matrixX[x, y2];
        float yLine = matrixY[x, y1] + matrixY[x, y2];
        
        //Debug.Log("Gay X: " + x + "Gay Y: " + y);
        vector3 = new Vector3(xLine / 2, yLine / 2, 0);
        Vector3 scale = transform.localScale;
        inst = Instantiate(Resources.Load("Horizontal"), vector3, Quaternion.identity) as GameObject;
        inst.transform.SetParent(canvas, false);

        scale.x = (y1 - y2) * 1.2f;
        inst.transform.localScale = scale;
    }
    public void CreateRectX(int x1, int y1, int x2, int y2, int v) 
    {

        CreateHorizontal(y1, v, x1);
        CreateVertical(x1, x2, v);
        CreateHorizontal(v, y2, x2);
        CreatePoint(x1, v);
        CreatePoint(x2, v);
    }
    public void CreateRectY(int x1, int y1, int x2, int y2, int h)
    {
        CreateVertical(x1, h, y1);
        CreateHorizontal(y1, y2, h);
        CreateVertical(x2, h, y2);
        CreatePoint(h, y1);
        CreatePoint(h, y2);
    }
    private void CreateMatrixX()
    {
        
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                
                matrixX[i, j] = -547 + j * 125;
            }
            
        }
    }
    private void CreateMatrixY()
    {
        
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                matrixY[i, j] = 679 - i * 125;
            }

        }
    }
}
