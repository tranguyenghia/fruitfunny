﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardController : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    FruitFN fruit1 = null, fruit2 = null;
    int press = 0;
    long star, end, total;
    public void OnPointerDown(PointerEventData eventData)
    {
        
        int value = PlayerPrefs.GetInt("Press");
        press = value;
        press++;
        if (press > 2)
        {
            press = 0;
        }
        PlayerPrefs.SetInt("Press", press);
        if (press == 1)
        {
            
            PlayerPrefs.Save();
            GamePlayController.instance.AudioButton(1);
            SetFruit1(this.gameObject.name);
            
            //string card1 = PlayerPrefs.GetString(this.gameObject.name);
            ////Debug.Log("Card1: " + card1);
            //int x = GetCardX(card1);
            //int y = GetCardY(card1);
            //int x1 = x + 1;
            //int y1 = y + 1;
            //string name1 = "Fruit" + GamePlayController.instance.matrix[x1, y1];
            //fruit1 = new FruitFN(x1, y1, name1);
            ////Debug.Log("Toa do button 1 vua nhan: x= " + x1 + "y= " + y1 + "qua ten la: " + name1);
            ////Debug.Log("Nhan: " + press);
            //FruitFN.instance.setFruit1(fruit1);
            //PlayerPrefs.SetString("card1", card1);
            //PlayerPrefs.Save();

            //Support Boom
            //if(PlayerPrefs.GetInt("Boom") == 1)
            //{
            //    FunctionController.instance.Boom(name1);  
            //}

            // Show Press Effect


        }
        if (press == 2)
        {
            GamePlayController.instance.AudioButton(1);
            SetFruit2(this.gameObject.name);
            
            //string card2 = PlayerPrefs.GetString(this.gameObject.name);
            //int x = GetCardX(card2);
            //int y = GetCardY(card2);
            //int x2 = x + 1;
            //int y2 = y + 1;
            //string name2 = "Fruit" + GamePlayController.instance.matrix[x2, y2];
            //fruit2 = new FruitFN(x2, y2, name2);
            ////Debug.Log("Toa do button 2 vua nhan: x= " + x2 + "y= " + y2 + "qua ten la: " + name2);
            //FruitFN.instance.setFruit2(fruit2);
            ////Fruit.instance.CheckTwoFruit();
            //PlayerPrefs.SetString("card2", card2);
            //PlayerPrefs.Save();     

        }  
    }

    private void SetFruit1(string name)
    {
        string card1 = PlayerPrefs.GetString(name);
        
        //Debug.Log("Card1: " + card1);
        int x = GetCardX(card1);
        int y = GetCardY(card1);
        int x1 = x + 1;
        int y1 = y + 1;
        int name1 = GamePlayController.instance.matrix[x1, y1];
        fruit1 = new FruitFN(x1, y1, name1);
        //Debug.Log("Toa do button 1 vua nhan: x= " + x1 + "y= " + y1 + "qua ten la: " + name1);
        //Debug.Log("Nhan: " + press);
        FruitFN.instance.setFruit1(fruit1);
        //PlayerPrefs.SetString("card1", card1);
        //PlayerPrefs.Save();
        GamePlayController.instance.xFruitSelected = x1;
        GamePlayController.instance.yFruitSelected = y1;
        PressEffect(x1, y1, true);

        //Support Boom
        if (PlayerPrefs.GetInt("Boom") == 1)
        {
            FunctionController.instance.Boom(name1);
            PlayerPrefs.SetInt("Press", 0);
        }
    }
    private void SetFruit2(string name)
    {
        string card2 = PlayerPrefs.GetString(name);
        int x = GetCardX(card2);
        int y = GetCardY(card2);
        int x2 = x + 1;
        int y2 = y + 1;
        int name2 = GamePlayController.instance.matrix[x2, y2];
        fruit2 = new FruitFN(x2, y2, name2);
        //Debug.Log("Toa do button 2 vua nhan: x= " + x2 + "y= " + y2 + "qua ten la: " + name2);
        FruitFN.instance.setFruit2(fruit2);
        //Fruit.instance.CheckTwoFruit();
        //PlayerPrefs.SetString("card2", card2);
        //PlayerPrefs.Save();
        GamePlayController.instance.xFruitSelected = x2;
        GamePlayController.instance.yFruitSelected = y2;
        PressEffect(x2, y2, true);

        //Support Boom
        if (PlayerPrefs.GetInt("Boom") == 1)
        {
            FunctionController.instance.Boom(name2);
            PlayerPrefs.SetInt("Press", 0);
        }
    }
    public void OnPointerUp(PointerEventData eventData)
    {  
        if(press == 2)
        {
            
            //FunctionController.instance.SetArrayTemp();
            //FunctionController.instance.SetMatrixRandom();

            PlayerPrefs.SetInt("Press", 0);
            PlayerPrefs.Save();
            //Debug.Log("Check in CardController" + Fruit.instance.CheckTwoFruit());
            //get fruit
            FruitFN fruit1 = FruitFN.instance.getFruit1();
            FruitFN fruit2 = FruitFN.instance.getFruit2();
            star = System.DateTime.Now.ToFileTimeUtc();
            if (FunctionController.instance.CheckTowCard())
            {

                // Destroy tow card
                FunctionController.instance.DestroyCardWithXY(fruit1.X, fruit1.Y);
                FunctionController.instance.DestroyCardWithXY(fruit2.X, fruit2.Y);
                GamePlayController.instance.PlayMode();
                GamePlayController.instance.AudioButton(2);
                //SetNullMatrix(fruit1.X, fruit1.Y, fruit2.X, fruit2.Y);

                //Find Way
                //if (FunctionController.instance.FindWay())
                //{
                //    //Debug.Log("Qua A: " + FunctionController.instance.fruitA);
                //    //Debug.Log("Qua B: " + FunctionController.instance.fruitB);
                //}
                //else
                //{
                //    FunctionController.instance.ReCreateFruit();
                //}
                //if (GamePlayController.instance.CountFruit() > 2)
                //{
                //    while (!FunctionController.instance.FindWay())
                //    {
                //        FunctionController.instance.ReCreateFruit();
                //    }
                //}
                //else
                //{

                //}
                
                FunctionController.instance.RecreateFruitGravity(GamePlayController.instance.gravityType);
                FunctionController.instance.LoopFindWay();

                //FunctionController.instance.GetMatrix();
                GamePlayController.instance.xFruitSelected = 0;
                GamePlayController.instance.yFruitSelected = 0;
            }
            else
            {
                // Hide Press Effect
                PressEffect(fruit1.X, fruit1.Y, false);
                PressEffect(fruit2.X, fruit2.Y, false);

                string name1 = GamePlayController.instance.matrixFruitObject[fruit2.X, fruit2.Y];
                SetFruit1(name1);
                PlayerPrefs.SetInt("Press", 1);
                
            }
            end = System.DateTime.Now.ToFileTimeUtc();
            //Debug.Log("Time" + (end - star));
           
        }               
    }
    private void PressEffect(int x, int y, bool type)
    {
        GameObject button = GameObject.Find(GamePlayController.instance.matrixButtonBGObject[x, y]);
        if (type)
        {
            button.transform.GetChild(0).gameObject.SetActive(false);
            button.transform.GetChild(2).gameObject.SetActive(true);
        }
        else
        {
            button.transform.GetChild(0).gameObject.SetActive(true);
            button.transform.GetChild(2).gameObject.SetActive(false);
        }
    }
    private int GetCardX(string card)
    {
        int x=0;
        string[] xy = card.Split(',');
        x = Int32.Parse(xy[0]);
        return x;
    }
    private int GetCardY(string card)
    {
        int y = 0;
        string[] xy = card.Split(',');
        y = Int32.Parse(xy[1]);
        return y;
    }
    //private void SetNullMatrix(int x1, int y1, int x2, int y2)
    //{
    //    GamePlayController.instance.matrix[x1, y1] = 0;
    //    GamePlayController.instance.matrix[x2, y2] = 0;
        
    //}

    // Use this for initialization
    void Start () {
        PlayerPrefs.SetInt("Press", 0);
        PlayerPrefs.Save();
	}
    
	
	// Update is called once per frame
	void Update () {
		
	}
}
