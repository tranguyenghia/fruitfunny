﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : MonoBehaviour {

    public Transform canvas;
    public GameObject[,] instance = new GameObject[10, 8];
    // Use this for initialization
    public int x;
    public int y;
    public string name;
    public Vector3 vector3;

                              
    public Fruit(int x, int y, string name, Vector3 vector3)
    {
        this.x = x;
        this.y = y;
        this.name = name;
        this.vector3 = vector3;
    }
    public void CreateCard()
    {
        //Debug.Log("Qua " + name + " x: " + x + " y:" + y + " vectorX: " + vector3.x + " vectorY: " + vector3.y);
        canvas = GameObject.FindGameObjectWithTag("Canvas1").GetComponent<Transform>();

        instance[x, y] = Instantiate(Resources.Load(name), vector3, Quaternion.identity) as GameObject;
        instance[x, y].transform.SetParent(canvas, false);
        instance[x, y].name = instance[x, y].GetInstanceID().ToString();
        PlayerPrefs.SetString(instance[x, y].name, x.ToString() + "," + y.ToString());
        PlayerPrefs.Save();
        
        GamePlayController.instance.matrixFruitObject[x + 1, y + 1] = instance[x, y].name;

        //Debug.Log("Qua " + (x + 1) + "-" + (y + 1) + instance[x, y].name);
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}

