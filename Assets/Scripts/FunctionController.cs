﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionController : MonoBehaviour
{
    public static FunctionController instance;
    //public string[,] matrix;
    public GameObject bubble;
    private int[] arrayTemp;
    private List<int> arrayCurrentFruit = new List<int>();
    private List<string> arrayNameFruit = new List<string>();
    private string[] arrayTotalNameFruit;
    //private List<GameObject> arrayObjectFruit = new List<GameObject>();

    //public string fruitA, fruitB;
    public int xA, yA, xB, yB;

    public bool searchNext = true;
    long start, end;

    // Use this for initialization

    void Start()
    {
        //matrix = new string[12, 10];
        arrayTemp = new int[81];
        arrayTotalNameFruit = new string[81];
        MakeInstance();
        //PlayerPrefs.SetString("FruitA", "0-0");
        //PlayerPrefs.Save();
        //PlayerPrefs.SetString("FruitB", "1-1");
        //PlayerPrefs.Save();
        xA = 0;
        yA = 0;
        xB = 1;
        yB = 1;
        
    }
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public bool FindWay()
    {

        //if (checkWay(PlayerPrefs.GetString("FruitA"), PlayerPrefs.GetString("FruitB")))
        //{
        if(checkWay(xA, yA, xB, yB))
        { 
            //Debug.Log("Way van the");
            return true;
        }
        else
        {
            // Debug.Log("Bat dau duyet");
            for (int i = 1; i < 11; i++)
            {
                for (int j = 1; j < 9; j++)
                {
                    FruitFN fruit1 = CreateFruitWithXY(i, j);
                    FruitFN.instance.setFruit1(fruit1);
                    if (fruit1.Name != 0)
                    {
                        for (int k = i; k < 11; k++)
                        {
                            if (k == i)
                            {
                                for (int l = j + 1; l < 9; l++)
                                {
                                    FruitFN fruit2 = CreateFruitWithXY(k, l);
                                    FruitFN.instance.setFruit2(fruit2);
                                    if (fruit2.Name != 0 && fruit1.Name == fruit2.Name)
                                    {
                                        if (FruitFN.instance.CheckTwoFruit() != -1)
                                        {
                                            //PlayerPrefs.SetString("FruitA", i.ToString() + "-" + j.ToString());
                                            //PlayerPrefs.Save();
                                            //PlayerPrefs.SetString("FruitB", k.ToString() + "-" + l.ToString());
                                            //PlayerPrefs.Save();
                                            //fruitA = i.ToString() + "-" + j.ToString();
                                            //fruitB = k.ToString() + "-" + l.ToString();
                                            xA = i;
                                            yA = j;
                                            xB = k;
                                            yB = l;
                                            return true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int l = 1; l < 9; l++)
                                {
                                    FruitFN fruit2 = CreateFruitWithXY(k, l);
                                    FruitFN.instance.setFruit2(fruit2);
                                    if (fruit2.Name != 0 && fruit1.Name == fruit2.Name)
                                    {
                                        if (FruitFN.instance.CheckTwoFruit() != -1)
                                        {
                                            //PlayerPrefs.SetString("FruitA", i.ToString() + "-" + j.ToString());
                                            //PlayerPrefs.Save();
                                            //PlayerPrefs.SetString("FruitB", k.ToString() + "-" + l.ToString());
                                            //PlayerPrefs.Save();
                                            //fruitA = i.ToString() + "-" + j.ToString();
                                            //fruitB = k.ToString() + "-" + l.ToString();
                                            xA = i;
                                            yA = j;
                                            xB = k;
                                            yB = l;
                                            return true;
                                        }
                                    }
                                }
                            }                           
                        }
                    }
                }
            }
            
            //Debug.Log("Da duyet het matrix de tim duong");
        }

        return false;

    }
    private bool checkWay(int xA, int yA, int xB, int yB)
    {
        //int xA = GetCardX(cardA);
        //int yA = GetCardY(cardA);
        //int xB = GetCardX(cardB);
        //int yB = GetCardY(cardB);
        FruitFN fruitA = CreateFruitWithXY(xA, yA);
        FruitFN fruitB = CreateFruitWithXY(xB, yB);

        if (fruitA.Name != 0 && fruitB.Name != 0 && fruitA.Name == fruitB.Name )
        {
            return true;
        }
        return false;
    }
    public void LoopFindWay()
    {
        FindWay();
        int count = 0;
        while (!FindWay()&& GamePlayController.instance.CountFruit()>2)
        {
            count++;
            Debug.Log("Lan chay thu :" + count);
            ReCreateFruit();
            FindWay();
        }
    }
    public void Search()
    {
        //if(fruitA == "0-0" && fruitB == "1-1")
        //{
        
        //Debug.Log("Qua 1: " + xA + "-" + yA + " Qua 2: " + xB + "-" + yB);
        searchNext = false;
        LoopFindWay();      
        //if (xA == 0 && yA == 0 && xB ==1 && yB == 1 && GamePlayController.instance.CountFruit() > 2)
        //{
        //    int count = 0;
        //    while (!FindWay())
        //    {
        //        count++;
        //        ReCreateFruit();
        //        Debug.Log("Lan chay thu: " + count);
        //    }
        //}
        //else
        //{
           
        //}
        //int xA = GetCardX(fruitA);
        //int yA = GetCardY(fruitA);
        //int xB = GetCardX(fruitB);
        //int yB = GetCardY(fruitB);
       Debug.Log("Qua 1: " + xA + "-" + yA + " Qua 2: " + xB + "-" + yB);
       if(GamePlayController.instance.CountFruit() > 1)
        {
            FruitFN fruit1 = CreateFruitWithXY(xA, yA);
            FruitFN.instance.setFruit1(fruit1);
            FruitFN fruit2 = CreateFruitWithXY(xB, yB);
            FruitFN.instance.setFruit2(fruit2);
            //Debug.Log("Qua 1: ten la: " + GamePlayController.instance.matrix[xA, yA] + " toa do: " + xA + "-" + yA +
            //    " Qua 2: ten la: " + GamePlayController.instance.matrix[xB, yB] + " toa do: " + xB + "-" + yB);
            if (CheckTowCard())
            {
                DestroyCardWithXY(xA, yA);
                DestroyCardWithXY(xB, yB);
                xA = 0;
                yA = 0;
                xB = 1;
                yB = 1;
                RecreateFruitGravity(GamePlayController.instance.gravityType);
                LoopFindWay();
                searchNext = true;
            }
        }
        

        //FindWay();
    }
    public bool CheckTowCard()
    {
        FruitFN fruit1 = FruitFN.instance.getFruit1();
        FruitFN fruit2 = FruitFN.instance.getFruit2();
        if (FruitFN.instance.CheckTwoFruit() != -1)
        {
            int check = FruitFN.instance.CheckTwoFruit();

            if (check >= 0 && check < 20)
            {
                int x1 = fruit1.X;
                int y1 = fruit1.Y, y2 = fruit2.Y;
                LineController.instance.CreateHorizontal(y1, y2, x1);
            }
            else if (check >= 20 && check < 40)
            {
                int x1 = fruit1.X, x2 = fruit2.X;
                int y1 = fruit1.Y;
                LineController.instance.CreateVertical(x1, x2, y1);
            }
            else if (check >= 40 && check < 60)
            {
                int v = check - 40;
                int x1 = fruit1.X, x2 = fruit2.X;
                int y1 = fruit1.Y, y2 = fruit2.Y;

                LineController.instance.CreateRectX(x1, y1, x2, y2, v);
            }
            else if (check >= 60 && check < 80)
            {
                int h = check - 60;
                int x1 = fruit1.X, x2 = fruit2.X;
                int y1 = fruit1.Y, y2 = fruit2.Y;
                LineController.instance.CreateRectY(x1, y1, x2, y2, h);
            }
            else if (check >= 80 && check < 100)
            {
                int v = check - 80;
                int x1 = fruit1.X, x2 = fruit2.X;
                int y1 = fruit1.Y, y2 = fruit2.Y;
                LineController.instance.CreateRectX(x1, y1, x2, y2, v);
            }
            else if (check >= 100 && check < 120)
            {
                int v = check - 100;
                int x1 = fruit1.X, x2 = fruit2.X;
                int y1 = fruit1.Y, y2 = fruit2.Y;
                LineController.instance.CreateRectX(x1, y1, x2, y2, v);
            }
            else if (check >= 120 && check < 140)
            {
                int h = check - 120;
                int x1 = fruit1.X, x2 = fruit2.X;
                int y1 = fruit1.Y, y2 = fruit2.Y;
                LineController.instance.CreateRectY(x1, y1, x2, y2, h);
            }
            else
            {
                int h = check - 140;
                int x1 = fruit1.X, x2 = fruit2.X;
                int y1 = fruit1.Y, y2 = fruit2.Y;
                LineController.instance.CreateRectY(x1, y1, x2, y2, h);
            }
            return true;
        }
        return false;
    }
    public void Boom(int name)
    {
        
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 9; j++)
            {
                int matrixName = GamePlayController.instance.matrix[i, j];

                if (name == matrixName)
                {
                    DestroyCardWithXY(i, j);
                }
            }
        }
        RecreateFruitGravity(GamePlayController.instance.gravityType);
        LoopFindWay();
        //GetMatrix();
        if (PlayerPrefs.GetInt("Boom") == 1)
        {
            PlayerPrefs.SetInt("Boom", 0);
            PlayerPrefs.Save();
            SupportController.instance.SetScaleBoomButton(1);
            GamePlayController.instance.xFruitSelected = 0;
            GamePlayController.instance.yFruitSelected = 0;
        }
    }

    private FruitFN CreateFruitWithXY(int x, int y)
    {
        //string name = PlayerPrefs.GetString(x.ToString() + y.ToString());
        //FruitFN fruit = new FruitFN(x, y, name);

        //return fruit;
        int name = GamePlayController.instance.matrix[x, y];
        //string fruitName;
        //if(name == 0)
        //{
        //    fruitName = "Null";
        //}
        //else
        //{
        //    fruitName = "Fruit" + name;
        //}
        FruitFN fruit = new FruitFN(x, y, name);
        return fruit;

    }
    private GameObject FindObjectFruitWithXY(int x, int y)
    {
        String nameObject = GamePlayController.instance.matrixFruitObject[x, y];
        GameObject fruit = GameObject.Find(nameObject);
        return fruit;
    }
    private GameObject FindObjectButtonWithXY(int x, int y)
    {
        String nameObject = GamePlayController.instance.matrixButtonBGObject[x, y];
        GameObject button = GameObject.Find(nameObject);
        return button;
    }
    private Vector3 GetVectorObjectWithXY(int x, int y, int type)
    {
        Vector3 vector3 = Vector3.zero;
        float right = -1;
        float top = -1;
        
        float row = 5;
        for (int i = 1; i < x + 1; i++)
        {
            if (i < 6)
            {
                row--;
                top = 1;
            }
            else
            {
                row++;
                top = -1;
            }
            float col = 4;
            for (int j = 1; j < y + 1; j++)
            {

                if (j < 5)
                {
                    col--;
                    right = -1;

                }
                else
                {
                    col++;
                    right = 1;
                }
                //type 1: fruit
                //type 2: buttonBackGround
                if(type == 1)
                {
                    vector3 = new Vector3(col * right * 125 - 47, row * top * 125 - 43 + 97, 0);
                }
                else
                {
                    vector3 = new Vector3(col * right * 125 - 45, row * top * 125 - 45 + 97, 0);
                }
                

                //matrix[i, j] = ran;
            }
        }
        return vector3;
    }
    

    public void DestroyCardWithXY(int x, int y)
    {
        if (!GamePlayController.instance.stopDestroy)
        {
            //Set Null Matrix
            GamePlayController.instance.matrix[x, y] = 0;
            //Debug.Log("Qua search x:" + x + " y:" + y);
            // Find fruit
            GameObject fruit = GameObject.Find(GamePlayController.instance.matrixFruitObject[x, y]);
            //Debug.Log("qua vua nhan" + fruit.name);

            // Create bubble effect
            Instantiate(bubble, fruit.transform.position, Quaternion.identity);

            // Destroy fruit
            Destroy(fruit);

            //find and destroy button backgroudxxxx
            DestroyButtonWithXY(x, y);
        }
        
    }

    private void DestroyFruitWithXY(int x, int y)
    {
        GameObject fruit = GameObject.Find(GamePlayController.instance.matrixFruitObject[x, y]);
        Destroy(fruit);
    }
    private void DestroyButtonWithXY(int x, int y)
    {
        GameObject button = GameObject.Find(GamePlayController.instance.matrixButtonBGObject[x, y]);
        Destroy(button);
    }
    private int GetCardX(string card)
    {
        int x = 0;
        string[] xy = card.Split('-');
        x = Int32.Parse(xy[0]);
        return x;
    }
    private int GetCardY(string card)
    {
        int y = 0;
        string[] xy = card.Split('-');
        y = Int32.Parse(xy[1]);
        return y;
    }

    public void GetMatrix()
    {
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 9; j++)
            {
                string name = GamePlayController.instance.matrixFruitObject[i, j];
                //string name = arrayTotalNameFruit[(i - 1) * 8 + j];
                Debug.Log("Qua: " + i + "-" + j + " la: " + name);
            }
            Debug.Log("Het Hang roi");
        }
    }
    private void SetArrayTemp()
    {
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 9; j++)
            {
                int name = GamePlayController.instance.matrix[i, j];

                arrayTemp[(i - 1) * 8 + j] = name;

                if(name != 0)
                {
                    arrayCurrentFruit.Add(name);
                    string nameObject = GamePlayController.instance.matrixFruitObject[i, j];                  
                    arrayNameFruit.Add(nameObject);
                    arrayTotalNameFruit[(i - 1) * 8 + j] = nameObject;
                    
                }
                else
                {
                    arrayTotalNameFruit[(i - 1) * 8 + j] = null;
                    //arrayObjectFruit.Add(null);
                }
                //PlayerPrefs.SetString(i.ToString() + j.ToString(), "Fruit");
                //PlayerPrefs.Save();


            }
            //Debug.Log("Het Hang roi");
        }
    }
    //public void GetArrayCF()
    //{
    //    SetArrayTemp();
    //    for(int i=0; i<arrayCurrentFruit.Count; i++)
    //    {
    //        Debug.Log(""+arrayCurrentFruit[i]);
    //    }
    //    Debug.Log("HETTTTTTTTT");
    //}
 
    private void SetMatrixRandom()
    {
        // swap random array  
        for (int i = 1; i < arrayTemp.Length; i++)
        {
            //int j = UnityEngine.Random.Range(1, 80);
            //if (arrayTemp[i] == 0 || arrayTemp[j] == 0)
            //{

            //    // Debug.Log("voi i :" + i + " va j: " + j + " thi khong lam gi ca");
            //}
            //else
            //{
            //    SwapArrayFuncition(i, j);
            //}
            if (arrayTemp[i] != 0)
            {
                int random = UnityEngine.Random.Range(0, arrayCurrentFruit.Count - 1);
                arrayTemp[i] = arrayCurrentFruit[random];
                arrayCurrentFruit.RemoveAt(random);

                arrayTotalNameFruit[i] = arrayNameFruit[random];
                arrayNameFruit.RemoveAt(random);


            }

        }
        //set matrix random
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 9; j++)
            {
                //string name = PlayerPrefs.GetString(i.ToString() + j.ToString());

                //PlayerPrefs.SetString(i.ToString() + j.ToString(), arrayTemp[(i - 1) * 8 + j]);
                GamePlayController.instance.matrix[i, j] = arrayTemp[(i - 1) * 8 + j];
                if(GamePlayController.instance.matrix[i,j] != 0)
                {
                    GameObject fruit = GameObject.Find(arrayTotalNameFruit[(i - 1) * 8 + j]);
                    fruit.transform.localPosition = GetVectorObjectWithXY(i, j, 1);

                    string nameObjectFruit = arrayTotalNameFruit[(i - 1) * 8 + j];
                    GamePlayController.instance.matrixFruitObject[i, j] = nameObjectFruit;
                    PlayerPrefs.SetString(nameObjectFruit, (i - 1).ToString() + "," + (j - 1).ToString());
                }
            }
        }


    }
    private void SwapArrayFuncition(int indexA, int indexB)
    {
        int temp = arrayTemp[indexA];
        arrayTemp[indexA] = arrayTemp[indexB];
        arrayTemp[indexB] = temp;
    }
    
    public void GravityVerticalCenter(int type)
    {
        // i is column
        for (int i = 1; i < 9; i++)
        {
            if(type == 1)
            {
                for (int j = 5; j > 1; j--)
                {
                    int fruit1 = GamePlayController.instance.matrix[j, i];
                    if (fruit1 == 0)
                    {
                        for (int k = j - 1; k > 0; k--)
                        {
                            int fruit2 = GamePlayController.instance.matrix[k, i];
                            if (fruit2 != 0)
                            {
                                GamePlayController.instance.matrix[j, i] = fruit2;
                                GamePlayController.instance.matrix[k, i] = 0;

                                //GameObject fruitObj1 = FindObjectButtonWithXY(j, i);
                                GameObject fruit = FindObjectFruitWithXY(k, i);
                                fruit.transform.localPosition = GetVectorObjectWithXY(j, i, 1);

                                GamePlayController.instance.matrixFruitObject[j, i] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[k, i] = null;
                                PlayerPrefs.SetString(fruit.name, (j-1).ToString() + "," + (i-1).ToString());

                                GameObject button = FindObjectButtonWithXY(k, i);
                                button.transform.localPosition = GetVectorObjectWithXY(j, i, 2);

                                GamePlayController.instance.matrixButtonBGObject[j, i] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[k, i] = null;
                                break;
                            }
                        }

                    }
                }
            }
            else if(type == 2)
            {
                for (int m = 6; m < 10; m++)
                {
                    int fruit1 = GamePlayController.instance.matrix[m, i];
                    if (fruit1 == 0)
                    {
                        for (int n = m + 1; n < 11; n++)
                        {
                            int fruit2 = GamePlayController.instance.matrix[n, i];
                            if (fruit2 != 0)
                            {
                                GamePlayController.instance.matrix[m, i] = fruit2;
                                GamePlayController.instance.matrix[n, i] = 0;

                                //GameObject fruitObj1 = FindObjectButtonWithXY(m, i);
                                GameObject fruit = FindObjectFruitWithXY(n, i);
                                fruit.transform.localPosition = GetVectorObjectWithXY(m, i, 1);

                                GamePlayController.instance.matrixFruitObject[m, i] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[n, i] = null;
                                PlayerPrefs.SetString(fruit.name, (m - 1).ToString() + "," + (i - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(n, i);
                                button.transform.localPosition = GetVectorObjectWithXY(m, i, 2);

                                GamePlayController.instance.matrixButtonBGObject[m, i] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[n, i] = null;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                for (int m = 6; m < 10; m++)
                {
                    int fruit1 = GamePlayController.instance.matrix[m, i];
                    if (fruit1 == 0)
                    {
                        for (int n = m + 1; n < 11; n++)
                        {
                            int fruit2 = GamePlayController.instance.matrix[n, i];
                            if (fruit2 != 0)
                            {
                                GamePlayController.instance.matrix[m, i] = fruit2;
                                GamePlayController.instance.matrix[n, i] = 0;

                                //GameObject fruitObj1 = FindObjectButtonWithXY(m, i);
                                GameObject fruit = FindObjectFruitWithXY(n, i);
                                fruit.transform.localPosition = GetVectorObjectWithXY(m, i, 1);

                                GamePlayController.instance.matrixFruitObject[m, i] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[n, i] = null;
                                PlayerPrefs.SetString(fruit.name, (m - 1).ToString() + "," + (i - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(n, i);
                                button.transform.localPosition = GetVectorObjectWithXY(m, i, 2);

                                GamePlayController.instance.matrixButtonBGObject[m, i] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[n, i] = null;
                                break;
                            }
                        }
                    }
                }
                for (int j = 5; j > 1; j--)
                {
                    int fruit1 = GamePlayController.instance.matrix[j, i];
                    if (fruit1 == 0)
                    {
                        for (int k = j - 1; k > 0; k--)
                        {
                            int fruit2 = GamePlayController.instance.matrix[k, i];
                            if (fruit2 != 0)
                            {
                                GamePlayController.instance.matrix[j, i] = fruit2;
                                GamePlayController.instance.matrix[k, i] = 0;

                                //GameObject fruitObj1 = FindObjectButtonWithXY(j, i);
                                GameObject fruit = FindObjectFruitWithXY(k, i);
                                fruit.transform.localPosition = GetVectorObjectWithXY(j, i, 1);

                                GamePlayController.instance.matrixFruitObject[j, i] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[k, i] = null;
                                PlayerPrefs.SetString(fruit.name, (j - 1).ToString() + "," + (i - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(k, i);
                                button.transform.localPosition = GetVectorObjectWithXY(j, i, 2);

                                GamePlayController.instance.matrixButtonBGObject[j, i] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[k, i] = null;
                                break;
                            }
                        }

                    }
                }
            }
            
        }
    }
    public void GravityHorizontalCenter(int type)
    {
        // i is row
        
        for (int i = 1; i < 11; i++)
        {
            if (type == 1)
            {
                for (int j = 5; j < 8; j++)
                {
                    int fruit1 = GamePlayController.instance.matrix[i, j];
                    if (fruit1 == 0)
                    {
                        for (int k = j + 1; k < 9; k++)
                        {
                            int fruit2 = GamePlayController.instance.matrix[i, k];
                            if (fruit2 != 0)
                            {
                                GamePlayController.instance.matrix[i, j] = fruit2;
                                GamePlayController.instance.matrix[i, k] = 0;

                                //GameObject fruitObj1 = FindObjectButtonWithXY(i, j);
                                GameObject fruit = FindObjectFruitWithXY(i, k);
                                fruit.transform.localPosition = GetVectorObjectWithXY(i, j, 1);

                                GamePlayController.instance.matrixFruitObject[i, j] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[i, k] = null;
                                PlayerPrefs.SetString(fruit.name, (i - 1).ToString() + "," + (j - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(i, k);
                                button.transform.localPosition = GetVectorObjectWithXY(i, j, 2);

                                GamePlayController.instance.matrixButtonBGObject[i, j] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[i, k] = null;
                                break;
                            }
                        }

                    }
                }
            }
            else if (type == 2)
            {
                for (int m = 4; m > 1; m--)
                {
                    int fruit1 = GamePlayController.instance.matrix[i, m];
                    if (fruit1 == 0)
                    {
                        for (int n = m - 1; n > 0; n--)
                        {
                            int fruit2 = GamePlayController.instance.matrix[i, n];
                            if (fruit2 != 0)
                            {
                                GamePlayController.instance.matrix[i, m] = fruit2;
                                GamePlayController.instance.matrix[i, n] = 0;

                                //GameObject fruitObj1 = FindObjectButtonWithXY(i, m);
                                GameObject fruit = FindObjectFruitWithXY(i, n);
                                fruit.transform.localPosition = GetVectorObjectWithXY(i, m, 1);

                                GamePlayController.instance.matrixFruitObject[i, m] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[i, n] = null;
                                PlayerPrefs.SetString(fruit.name, (i - 1).ToString() + "," + (m - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(i, n);
                                button.transform.localPosition = GetVectorObjectWithXY(i, m, 2);

                                GamePlayController.instance.matrixButtonBGObject[i, m] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[i, n] = null;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                for (int j = 5; j < 8; j++)
                {
                    int fruit1 = GamePlayController.instance.matrix[i, j];
                    if (fruit1 == 0)
                    {
                        for (int k = j + 1; k < 9; k++)
                        {
                            int fruit2 = GamePlayController.instance.matrix[i, k];
                            if (fruit2 != 0)
                            {
                                GamePlayController.instance.matrix[i, j] = fruit2;
                                GamePlayController.instance.matrix[i, k] = 0;

                                //GameObject fruitObj1 = FindObjectButtonWithXY(i, j);
                                GameObject fruit = FindObjectFruitWithXY(i, k);
                                fruit.transform.localPosition = GetVectorObjectWithXY(i, j, 1);

                                GamePlayController.instance.matrixFruitObject[i, j] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[i, k] = null;
                                PlayerPrefs.SetString(fruit.name, (i - 1).ToString() + "," + (j - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(i, k);
                                button.transform.localPosition = GetVectorObjectWithXY(i, j, 2);

                                GamePlayController.instance.matrixButtonBGObject[i, j] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[i, k] = null;
                                break;
                            }
                        }

                    }
                }
                for (int m = 4; m > 1; m--)
                {
                    int fruit1 = GamePlayController.instance.matrix[i, m];
                    if (fruit1 == 0)
                    {
                        for (int n = m - 1; n > 0; n--)
                        {
                            int fruit2 = GamePlayController.instance.matrix[i, n];
                            if (fruit2 != 0)
                            {
                                GamePlayController.instance.matrix[i, m] = fruit2;
                                GamePlayController.instance.matrix[i, n] = 0;

                                //GameObject fruitObj1 = FindObjectButtonWithXY(i, m);
                                GameObject fruit = FindObjectFruitWithXY(i, n);
                                fruit.transform.localPosition = GetVectorObjectWithXY(i, m, 1);

                                GamePlayController.instance.matrixFruitObject[i, m] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[i, n] = null;
                                PlayerPrefs.SetString(fruit.name, (i - 1).ToString() + "," + (m - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(i, n);
                                button.transform.localPosition = GetVectorObjectWithXY(i, m, 2);

                                GamePlayController.instance.matrixButtonBGObject[i, m] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[i, n] = null;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    public void GravityVerticalEdge(int type)
    {
        // i is column
        
        for (int i = 1; i < 9; i++)
        {
            if (type == 1)
            {
                for (int j = 1; j < 10; j++)
                {
                    //string name1 = PlayerPrefs.GetString(j.ToString() + i.ToString());
                    int fruit1 = GamePlayController.instance.matrix[j, i];
                    if (fruit1 == 0)
                    {
                        for (int k = j + 1; k < 11; k++)
                        {
                            //string name2 = PlayerPrefs.GetString(k.ToString() + i.ToString());
                            int fruit2 = GamePlayController.instance.matrix[k, i];
                            if (fruit2 != 0)
                            {
                                //PlayerPrefs.SetString(j.ToString() + i.ToString(), name2);
                                //PlayerPrefs.SetString(k.ToString() + i.ToString(), "Null");
                                GamePlayController.instance.matrix[j, i] = fruit2;
                                GamePlayController.instance.matrix[k, i] = 0;

                                GameObject fruit = FindObjectFruitWithXY(k, i);
                                fruit.transform.localPosition = GetVectorObjectWithXY(j, i, 1);

                                GamePlayController.instance.matrixFruitObject[j, i] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[k, i] = null;
                                PlayerPrefs.SetString(fruit.name, (j - 1).ToString() + "," + (i - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(k, i);
                                button.transform.localPosition = GetVectorObjectWithXY(j, i, 2);

                                GamePlayController.instance.matrixButtonBGObject[j, i] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[k, i] = null;
                                break;
                            }
                        }

                    }
                }
            }
            else if (type == 2)
            {
                for (int m = 10; m > 1; m--)
                {
                    //string name1 = PlayerPrefs.GetString(m.ToString() + i.ToString());
                    int fruit1 = GamePlayController.instance.matrix[m, i];
                    if (fruit1 == 0)
                    {
                        for (int n = m - 1; n > 0; n--)
                        {
                            //string name2 = PlayerPrefs.GetString(n.ToString() + i.ToString());
                            int fruit2 = GamePlayController.instance.matrix[n, i];
                            if (fruit2 != 0)
                            {
                                //PlayerPrefs.SetString(m.ToString() + i.ToString(), name2);
                                //PlayerPrefs.SetString(n.ToString() + i.ToString(), "Null");
                                GamePlayController.instance.matrix[m, i] = fruit2;
                                GamePlayController.instance.matrix[n, i] = 0;

                                GameObject fruit = FindObjectFruitWithXY(n, i);
                                fruit.transform.localPosition = GetVectorObjectWithXY(m, i, 1);

                                GamePlayController.instance.matrixFruitObject[m, i] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[n, i] = null;
                                PlayerPrefs.SetString(fruit.name, (m - 1).ToString() + "," + (i - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(n, i);
                                button.transform.localPosition = GetVectorObjectWithXY(m, i, 2);

                                GamePlayController.instance.matrixButtonBGObject[m, i] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[n, i] = null;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                for (int j = 1; j < 5; j++)
                {
                    //string name1 = PlayerPrefs.GetString(j.ToString() + i.ToString());
                    int fruit1 = GamePlayController.instance.matrix[j, i];
                    if (fruit1 == 0)
                    {
                        for (int k = j + 1; k < 6; k++)
                        {
                            //string name2 = PlayerPrefs.GetString(k.ToString() + i.ToString());
                            int fruit2 = GamePlayController.instance.matrix[k, i];
                            if (fruit2 != 0)
                            {
                                //PlayerPrefs.SetString(j.ToString() + i.ToString(), name2);
                                //PlayerPrefs.SetString(k.ToString() + i.ToString(), "Null");
                                GamePlayController.instance.matrix[j, i] = fruit2;
                                GamePlayController.instance.matrix[k, i] = 0;

                                GameObject fruit = FindObjectFruitWithXY(k, i);
                                fruit.transform.localPosition = GetVectorObjectWithXY(j, i, 1);

                                GamePlayController.instance.matrixFruitObject[j, i] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[k, i] = null;
                                PlayerPrefs.SetString(fruit.name, (j - 1).ToString() + "," + (i - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(k, i);
                                button.transform.localPosition = GetVectorObjectWithXY(j, i, 2);

                                GamePlayController.instance.matrixButtonBGObject[j, i] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[k, i] = null;
                                break;
                            }
                        }

                    }
                }
                for (int m = 10; m > 6; m--)
                {
                    //string name1 = PlayerPrefs.GetString(m.ToString() + i.ToString());
                    int fruit1 = GamePlayController.instance.matrix[m, i];
                    if (fruit1 == 0)
                    {
                        for (int n = m - 1; n > 5; n--)
                        {
                            //string name2 = PlayerPrefs.GetString(n.ToString() + i.ToString());
                            int fruit2 = GamePlayController.instance.matrix[n, i];
                            if (fruit2 != 0)
                            {
                                //PlayerPrefs.SetString(m.ToString() + i.ToString(), name2);
                                //PlayerPrefs.SetString(n.ToString() + i.ToString(), "Null");
                                GamePlayController.instance.matrix[m, i] = fruit2;
                                GamePlayController.instance.matrix[n, i] = 0;

                                GameObject fruit = FindObjectFruitWithXY(n, i);
                                fruit.transform.localPosition = GetVectorObjectWithXY(m, i, 1);

                                GamePlayController.instance.matrixFruitObject[m, i] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[n, i] = null;
                                PlayerPrefs.SetString(fruit.name, (m - 1).ToString() + "," + (i - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(n, i);
                                button.transform.localPosition = GetVectorObjectWithXY(m, i, 2);

                                GamePlayController.instance.matrixButtonBGObject[m, i] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[n, i] = null;
                                break;
                            }
                        }
                    }
                }
            }
            
        }
    }
    public void GravityHorizontalEdge(int type)
    {
        // i is row
        
        for (int i = 1; i < 11; i++)
        {
            if (type == 1)
            {
                for (int j = 8; j > 1; j--)
                {
                    //string name1 = PlayerPrefs.GetString(i.ToString() + j.ToString());
                    int fruit1 = GamePlayController.instance.matrix[i, j];
                    if (fruit1 == 0)
                    {
                        for (int k = j - 1; k > 0; k--)
                        {
                            //string name2 = PlayerPrefs.GetString(i.ToString() + k.ToString());
                            int fruit2 = GamePlayController.instance.matrix[i, k];
                            if (fruit2 != 0)
                            {
                                //PlayerPrefs.SetString(i.ToString() + j.ToString(), name2);
                                //PlayerPrefs.SetString(i.ToString() + k.ToString(), "Null");
                                GamePlayController.instance.matrix[i, j] = fruit2;
                                GamePlayController.instance.matrix[i, k] = 0;

                                GameObject fruit = FindObjectFruitWithXY(i, k);
                                fruit.transform.localPosition = GetVectorObjectWithXY(i, j, 1);

                                GamePlayController.instance.matrixFruitObject[i, j] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[i, k] = null;
                                PlayerPrefs.SetString(fruit.name, (i - 1).ToString() + "," + (j - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(i, k);
                                button.transform.localPosition = GetVectorObjectWithXY(i, j, 2);

                                GamePlayController.instance.matrixButtonBGObject[i, j] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[i, k] = null;

                                break;
                            }
                        }

                    }
                }
            }
            else if (type == 2)
            {
                for (int m = 1; m < 8; m++)
                {
                    //string name1 = PlayerPrefs.GetString(i.ToString() + m.ToString());
                    int fruit1 = GamePlayController.instance.matrix[i, m];
                    if (fruit1 == 0)
                    {
                        for (int n = m + 1; n < 9; n++)
                        {
                            //string name2 = PlayerPrefs.GetString(i.ToString() + n.ToString());
                            int fruit2 = GamePlayController.instance.matrix[i, n];
                            if (fruit2 != 0)
                            {
                                //PlayerPrefs.SetString(i.ToString() + m.ToString(), name2);
                                //PlayerPrefs.SetString(i.ToString() + n.ToString(), "Null");
                                GamePlayController.instance.matrix[i, m] = fruit2;
                                GamePlayController.instance.matrix[i, n] = 0;

                                GameObject fruit = FindObjectFruitWithXY(i, n);
                                fruit.transform.localPosition = GetVectorObjectWithXY(i, m, 1);

                                GamePlayController.instance.matrixFruitObject[i, m] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[i, n] = null;
                                PlayerPrefs.SetString(fruit.name, (i - 1).ToString() + "," + (m - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(i, n);
                                button.transform.localPosition = GetVectorObjectWithXY(i, m, 2);

                                GamePlayController.instance.matrixButtonBGObject[i, m] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[i, n] = null;

                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                for (int j = 8; j > 5; j--)
                {
                    //string name1 = PlayerPrefs.GetString(i.ToString() + j.ToString());
                    int fruit1 = GamePlayController.instance.matrix[i, j];
                    if (fruit1 == 0)
                    {
                        for (int k = j - 1; k > 4; k--)
                        {
                            //string name2 = PlayerPrefs.GetString(i.ToString() + k.ToString());
                            int fruit2 = GamePlayController.instance.matrix[i, k];
                            if (fruit2 != 0)
                            {
                                //PlayerPrefs.SetString(i.ToString() + j.ToString(), name2);
                                //PlayerPrefs.SetString(i.ToString() + k.ToString(), "Null");
                                GamePlayController.instance.matrix[i, j] = fruit2;
                                GamePlayController.instance.matrix[i, k] = 0;

                                GamePlayController.instance.matrix[i, j] = fruit2;
                                GamePlayController.instance.matrix[i, k] = 0;

                                GameObject fruit = FindObjectFruitWithXY(i, k);
                                fruit.transform.localPosition = GetVectorObjectWithXY(i, j, 1);

                                GamePlayController.instance.matrixFruitObject[i, j] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[i, k] = null;
                                PlayerPrefs.SetString(fruit.name, (i - 1).ToString() + "," + (j - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(i, k);
                                button.transform.localPosition = GetVectorObjectWithXY(i, j, 2);

                                GamePlayController.instance.matrixButtonBGObject[i, j] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[i, k] = null;
                                break;
                            }
                        }

                    }
                }
                for (int m = 1; m < 4; m++)
                {
                    //string name1 = PlayerPrefs.GetString(i.ToString() + m.ToString());
                    int fruit1 = GamePlayController.instance.matrix[i, m];
                    if (fruit1 == 0)
                    {
                        for (int n = m + 1; n < 5; n++)
                        {
                            //string name2 = PlayerPrefs.GetString(i.ToString() + n.ToString());
                            int fruit2 = GamePlayController.instance.matrix[i, n];
                            if (fruit2 != 0)
                            {
                                //PlayerPrefs.SetString(i.ToString() + m.ToString(), name2);
                                //PlayerPrefs.SetString(i.ToString() + n.ToString(), "Null");
                                GamePlayController.instance.matrix[i, m] = fruit2;
                                GamePlayController.instance.matrix[i, n] = 0;

                                GameObject fruit = FindObjectFruitWithXY(i, n);
                                fruit.transform.localPosition = GetVectorObjectWithXY(i, m, 1);

                                GamePlayController.instance.matrixFruitObject[i, m] = fruit.name;
                                GamePlayController.instance.matrixFruitObject[i, n] = null;
                                PlayerPrefs.SetString(fruit.name, (i - 1).ToString() + "," + (m - 1).ToString());

                                GameObject button = FindObjectButtonWithXY(i, n);
                                button.transform.localPosition = GetVectorObjectWithXY(i, m, 2);

                                GamePlayController.instance.matrixButtonBGObject[i, m] = button.name;
                                GamePlayController.instance.matrixButtonBGObject[i, n] = null;

                                break;
                            }
                        }
                    }
                }
            }
            
        }
    }

    public bool RecreateFruitGravity(int type)
    {
        
        switch (type)
        {
            case 0:
                return true;
                break;
            case 1:
                GravityHorizontalCenter(1);
                break;
            case 2:
                GravityHorizontalCenter(2);
                break;
            case 3:
                GravityVerticalCenter(1);
                break;
            case 4:
                GravityVerticalCenter(2);
                break;
            case 5:
                GravityHorizontalCenter(3);
                break;
            case 6:
                GravityVerticalCenter(3);
                break;
            case 7:
                GravityHorizontalCenter(3);
                GravityVerticalCenter(3);
               
                break;
            case 10:
                break;
            case 11:
                GravityHorizontalEdge(1);
                break;
            case 12:
                GravityHorizontalEdge(2);
                break;
            case 13:
                GravityVerticalEdge(1);
                break;
            case 14:
                GravityVerticalEdge(2);
                break;
            case 15:
                GravityHorizontalEdge(3);
                break;
            case 16:
                GravityVerticalEdge(3);
                break;
            case 17:
                GravityHorizontalEdge(3);
                GravityVerticalEdge(3);
                break;
        }
        ResetOrderForButton();      
        return false;

    }
    public void ReCreateFruit()
    {
        SetArrayTemp();
        SetMatrixRandom();

        //Vector3 vector3;
        //float right = -1;
        //float top = -1;
        
        //float row = 5;
        //for (int i = 1; i < 11; i++)
        //{
        //    if (i < 6)
        //    {
        //        row--;
        //        top = 1;
        //    }
        //    else
        //    {
        //        row++;
        //        top = -1;
        //    }
        //    float col = 4;
        //    for (int j = 1; j < 9; j++)
        //    {

        //        if (j < 5)
        //        {
        //            col--;
        //            right = -1;

        //        }
        //        else
        //        {
        //            col++;
        //            right = 1;
        //        }
        //        DestroyFruitWithXY(i, j);
        //        vector3 = new Vector3(col * right * 125 - 55, row * top * 125 - 40 + 97, 0);

        //        int name = GamePlayController.instance.matrix[i, j];
        //        //Debug.Log("Tao Lai Qua voi i = " + i + " j= " + j + " name la: " + name);
        //        if (name != 0)
        //        {
        //            string fruitName= "Fruit" + name;
        //            Fruit fruit = new Fruit(i - 1, j - 1, fruitName, vector3);
        //            fruit.CreateCard();
        //        }

        //        //matrix[i, j] = ran;
        //    }
        //}
    }
    private void ResetOrderForButton()
    {
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 9; j++)
            {
                if (GamePlayController.instance.matrix[i, j] != 0)
                {
                    GameObject button = FindObjectButtonWithXY(i, j);
                    button.transform.SetSiblingIndex((i - 1) * 8 + j);
                }            
                //arrayTemp[(i - 1) * 8 + j] = name;
                //PlayerPrefs.SetString(i.ToString() + j.ToString(), "Fruit");
                //PlayerPrefs.Save();
            }
            //Debug.Log("Het Hang roi");
        }
    }
}
